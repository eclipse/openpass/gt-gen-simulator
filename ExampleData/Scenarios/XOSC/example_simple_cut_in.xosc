<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (c) 2024, Ansys, Inc. -->
<OpenScenario>
    <FileHeader revMajor="1" revMinor="1"
        date="2025-01-10T10:00:00"
        description="Simple Cut-in Scenario without Parameteration. Both Ego and Traffic Vehicle are controlled by the simulator."
        author="Ansys AG">
    </FileHeader>
    <CatalogLocations>
        <VehicleCatalog>
            <Directory path="./Catalogs/Vehicles"/>
        </VehicleCatalog>
    </CatalogLocations>
    <RoadNetwork>
        <LogicFile filepath="../../Maps/simple_road_straight.xodr"/>
    </RoadNetwork>
    <Entities>
        <ScenarioObject name="Ego">
            <!--  Scenario must contain the "Ego" as the host behicle. -->
            <CatalogReference catalogName="VehicleCatalog" entryName="Car"></CatalogReference>
        </ScenarioObject>
        <ScenarioObject name="TrafficVehicle">
            <CatalogReference catalogName="VehicleCatalog" entryName="Van"></CatalogReference>
        </ScenarioObject>
    </Entities>
    <Storyboard>
        <Init>
            <Actions>
                <Private entityRef="Ego">
                    <PrivateAction>
                        <TeleportAction>
                            <Position>
                                <LanePosition roadId="0" laneId="-2" offset="0.0" s="5.0"></LanePosition>
                            </Position>
                        </TeleportAction>
                    </PrivateAction>
                    <PrivateAction>
                        <LongitudinalAction>
                            <SpeedAction>
                                <SpeedActionDynamics dynamicsShape="step" dynamicsDimension="time" value="0.0"/>
                                <SpeedActionTarget>
                                    <AbsoluteTargetSpeed value="13.8889"/>
                                </SpeedActionTarget>
                            </SpeedAction>
                        </LongitudinalAction>
                    </PrivateAction>
                </Private>
                <Private entityRef="TrafficVehicle">
                    <PrivateAction>
                        <TeleportAction>
                            <Position>
                                <!-- The Traffic vehicle is placed on the right lane with an offset of 0.0 and a
                                longitudinal position of 200.0.-->
                                <LanePosition roadId="0" laneId="-1" offset="0.0" s="60.0"></LanePosition>
                            </Position>
                        </TeleportAction>
                    </PrivateAction>
                    <PrivateAction>
                        <LongitudinalAction>
                            <SpeedAction>
                                <SpeedActionDynamics dynamicsShape="step" dynamicsDimension="time" value="0"/>
                                <SpeedActionTarget>
                                    <RelativeTargetSpeed entityRef="Ego" value="${-20.0 / 3.6}"
                                        speedTargetValueType="delta" continuous="false"/>
                                </SpeedActionTarget>
                            </SpeedAction>
                        </LongitudinalAction>
                    </PrivateAction>
                </Private>
            </Actions>
        </Init>
        <Story name="CutInStory">
            <Act name="CutInAct">
                <ManeuverGroup maximumExecutionCount="1" name="CutInManeuverGroup">
                    <Actors selectTriggeringEntities="false">
                        <EntityRef entityRef="TrafficVehicle"/>
                    </Actors>
                    <Maneuver name="CutInManeuver">
                        <Event name="CutInEvent" priority="override">
                            <Action name="CutInAction">
                                <PrivateAction>
                                    <LateralAction>
                                        <LaneChangeAction>
                                            <!-- The Traffic vehicle changes to the lane of ego vehicle within 3
                                            seconds.-->
                                            <LaneChangeActionDynamics dynamicsShape="cubic" value="3"
                                                dynamicsDimension="time"/>
                                            <LaneChangeTarget>
                                                <RelativeTargetLane entityRef="Ego" value="0"/>
                                            </LaneChangeTarget>
                                        </LaneChangeAction>
                                    </LateralAction>
                                </PrivateAction>
                            </Action>
                            <StartTrigger>
                                <ConditionGroup>
                                    <Condition name="AfterTime" delay="0" conditionEdge="rising">
                                        <ByValueCondition>
                                            <SimulationTimeCondition value="5" rule="greaterThan"/>
                                        </ByValueCondition>
                                    </Condition>
                                </ConditionGroup>
                            </StartTrigger>
                        </Event>
                    </Maneuver>
                </ManeuverGroup>
            </Act>
        </Story>
        <StopTrigger>
            <ConditionGroup>
                <Condition name="End" delay="0" conditionEdge="rising">
                    <ByValueCondition>
                        <SimulationTimeCondition value="10" rule="greaterThan"/>
                    </ByValueCondition>
                </Condition>
            </ConditionGroup>
        </StopTrigger>
    </Storyboard>
</OpenScenario>
