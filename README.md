# GT-Gen-Simulator

GT-Gen (stands for Ground Truth Generator) is a simulator designed to accelerate the development and testing of Advanced Driver-Assistance System (ADAS) and Autonomous Driving (AD) functions. It allows engineers to test and validate autonomous software functions in a virtual environment, eliminating real-world risks. This simulated environment called Ground-Truth is created in the form of an ASAM Open Simulation Interface (OSI) generated from a speciﬁc scenario (ASAM OpenSCENARIO) and a speciﬁc map (ASAM OpenDRIVE).

GT-Gen can be employed in two ways: as a standalone application designed to function alongside a Command-Line Interface tool (CLI), and, as a shared library that can be integrated into custom simulation applications.


## Getting Started

### Full Documentation
For detailed documentation, please visit the [documentation page](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/SUMMARY.md?ref_type=heads). The page contains comprehensive information about the simulator, including advanced usage, FAQs, and troubleshooting tips.

### Download Prebuilt Packages

To get started quickly, download the latest prebuilt packages from [Releases Page](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/releases). Prebuilt packages contain everything you need to run the project without manual compilation.

### Install and Run an Example Scenario

Refer to this [page](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/getting_started.md) for detailed instructions on installing the prebuilt package and running the initial example scenario.

## Build GTGenSimulator - Quick Guide

### Prerequisites
1. Bazel
   - Install Bazel: https://bazel.build/install
   - Install Bazel Command-Line Completion : https://bazel.build/install/completion
   - Bazel formatter: https://github.com/bazelbuild/buildtools/blob/master/buildifier/README.md
2. C++ build environment
3. Third-party dependencies can be found in the subfolder `third_party`
4. For additional information: see [dockerfile](utils/Dockerfile)

### Build and Run Unit-tests

```bash
# Build and test in the debug mode
bazel build --config=gt_gen //Cli/... //Simulator/...
bazel test --config=gt_gen //Cli/... //Simulator/...

# Build and test in the release mode
bazel build --config=gt_gen_release  //Cli/... //Simulator/...
bazel test  --config=gt_gen_release  //Cli/... //Simulator/...

# Run sanitizer checks
bazel build --config=gt_gen_san  //Cli/... //Simulator/...
bazel test  --config=gt_gen_san  //Cli/... //Simulator/...
```


## Developer Notes

### Pre-Merge Checks
The `style_and_lint_checks` step in the [CI job](utils/ci/Jenkinsfile) ensures code quality and consistency. It perform a series of checks on each merge request. Checks Included:
1. clang-format checks
2. bazel buildifier checks
3. shell script formatting

#### Use script locally:
1. Prerequisites: ensure have the necessary tools installed: see [dockerfile](utils/Dockerfile)
2. Run the script locally:
   ```sh
   ./utils/ci/scripts/check_and_fix/check_and_fix.sh
   ```

### Code-coverage

The code-coverage report can be accessed at the following link: [code-coverage report](https://ci.eclipse.org/openpass/job/GT-Gen-Simulator-PreMerge-Gate/job/main/lastSuccessfulBuild/artifact/artifacts/_site/bazel-coverage/index.html). This report is automatically updated after each commit is merged into the `main` branch and is also included in the artifacts for each tagged release.

To generate the code-coverage report locally, you can find the report under `bazel-coverage/index.html` by following the instructions below:

#### Use script locally:
- For native Linux systems:
```bash
./utils/ci/scripts/build_and_test/code_coverage.sh
```

### Tests Report
Access the detailed Tests report at the following link: [tests report](https://ci.eclipse.org/openpass/job/GT-Gen-Simulator-PreMerge-Gate/job/main/lastSuccessfulBuild/artifact/artifacts/_site/test_results.html). This report summarises the outcomes of unit tests, including counts of passed, failed, disabled, and errored tests, alongside total execution time. This report is automatically updated after each commit is merged into the `main` branch and is also included in the artifacts for each tagged release.

#### Use script locally:

To generate the test locally, you can find the report under `test_results.html` by following the instructions below:

- For native Linux systems:
```bash
./utils/ci/scripts/build_and_test/generate_test_report.sh
```


### Versioning
Versioning should follow the [Semantic Versioning](https://semver.org/) pattern `MAJOR.MINOR.PATCH`.
Currently this is a manual task that needs to be approved as a part of a code review.
For each release, ensure the associated version is updated in the relevant version files:
1. gt-gen-cli: `Cli/version.bzl`
2. gt-gen-simulator: `Simulator/LibSimCore/version.bzl`
3. create a new release tag starting with `v`

In the following, a coarse guideline is given for when to increment the according fields:

**Major**: Even small changes to a public header will most likely result in an increase of this number if they are breaking changes.
As this is probably the most common case, MAJOR will therefore be incremented relatively frequently compared to other libraries.

**Minor**: This increments, if changes are ABI compatible, i.e. independent of the rest of other mantle definitions.
It is expected that this does not happen very often at the current state of development.
This includes features related to testing, such as enhancing mocks, adding tests, etc., and
features related to chore tasks, such as CI scripts, CMake enhancements, formatters, etc.

**Patch**:
This increments for tasks such as fixing typos in the documentation (as long as they don't change the semantic of a function call!),
fixing bugs related to testing, and
fixing bugs related to chore tasks.



### Commit Message
Commit messages should follow the guidelines from [Conventional Commits](https://www.conventionalcommits.org/).

**Type**:
The commit messages are prefixed with a type descriptor that communicates the type of change.
The following is a summary of the possible types for a commit.
1. `feat:` introduces a new feature to the codebase (this correlates with **MINOR** in Semantic Versioning).
2. `fix:` patches a bug in the codebase (this correlates with **PATCH** in Semantic Versioning).
3. Appending `!` after the type/scope specifies that it introduces a breaking API change (correlating with **MAJOR** in Semantic Versioning).
4. `chore:` indicates no production code changes. These are typically maintenance tasks. Subtypes can be added: `chore(build):`, `chore(ci):`, `chore(style):`
5. Other types can also be used: `test:`, `docs:`, `perf:`, `refactor:`

**Footer**
(optional) Any additional information about the commit, such as referencing issue tracker IDs.
