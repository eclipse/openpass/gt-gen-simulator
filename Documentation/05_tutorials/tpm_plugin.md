## GT-Gen TPM Plugin Tutorial: Loading and Implementing TPM in Simulations

A Traffic Participant Model (TPM) represents the behavior and dynamics of entities in traffic simulations. TPMs interact with the simulation environment via the OpenSimulationInterface (OSI). Traffic participant models interact with the simulation environment by receiving SensorView data and can communicate their state through traffic updates. They may also receive traffic commands to influence their behavior. Find more details in [OSI traffic_participant](https://opensimulationinterface.github.io/osi-documentation/#_traffic_participant).

GT-Gen implements a flexible plugin architecture that enables the dynamic loading of Traffic Participant Model (TPM) plugins during runtime. This tutorial outlines how to implement a TPM module, load it as a plugin, and use it in your simulation projects.



### Overview

1. Implement a TPM Module: Create a TPM module following the plugin interface `ITrafficParticipantModel` found [here](https://gitlab.eclipse.org/eclipse/openpass/osi-traffic-participant)
2. Load TPM Plugin: Integrate the TPM plugin within an OpenSCENARIO file.
3. Configuration: Utilize the UserSettings file for plugin dir configuration.
4. Integration & Testing: Conduct integration and testing to ensure proper functioning.

Sample Scenario and UserSettings files are available in the [ExampleData/TpmPlugin](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/tree/main/ExampleData/TpmPlugin) directory.


### ITrafficParticipantModel Interface


The `ITrafficParticipantModel` interface serves as a foundational contract for all TPM plugins, ensuring consistent interaction within the GT-Gen-Core. It defines essential methods such as `Update`, which processes `OSI::SensorData` and updates the traffic participant's state `osi3::TrafficUpdate`, and `HasFinished`, indicating whether the model's task is complete.


```c++
class ITrafficParticipantModel
{
public:
  ITrafficParticipantModel() = default;
  virtual ~ITrafficParticipantModel() = default;

  /// @brief Identifies the OSI entity from osi3::GroundTruth entities in the provided \p sensor_view and
  ///        provides a new state with an osi3::TrafficUpdate.
  /// @param delta_time  Delta time between the step in milliseconds.
  /// @param sensor_view osi3::SensorView containing osi3::GroundTruth containing all the entities
  ///                    (including the controlled entity).
  virtual osi3::TrafficUpdate Update(const std::chrono::milliseconds& delta_time,
                                     const osi3::SensorView& sensor_view) = 0;

  /// @brief Indicates that the controlled entity will be updated or not.
  /// @return true if the entity will not be updated in future cycles, i.e. when the road has ended. False otherwise.
  virtual bool HasFinished() const = 0;
};


```

### Plugin Loader

GT-Gen-Core includes a plugin loader that dynamically loads shared library plugins at runtime, creating instances through a defined factory function, **`CreateTpm`**. The implementation of this loader can be found in the [TrafficParticipantControlUnit](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/blame/main/Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.cpp).




###  Implementing a TPM Example


An example TPM module is provided for reference and is located in the [`@gt_gen_core://Core/Environment/Controller/Internal/ControlUnits:traffic_participant_model_example`](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/blob/main/Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.h). This example showcases a simple model setting a constant position for the controlled object.


```c++
// file: @gt-gen-core//Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.h
class TpmExample : public ITrafficParticipantModel
{

    static std::shared_ptr<ITrafficParticipantModel> Create(const std::map<std::string, std::string>& parameters);

    osi3::TrafficUpdate Update(const std::chrono::milliseconds& delta_time,
                               const osi3::SensorView& sensor_view) override;

    bool HasFinished() const override;

    ...
```

```c++
// file @gt-gen-core//Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.cpp
osi3::TrafficUpdate TpmExample::Update([[maybe_unused]] const std::chrono::milliseconds& delta_time,
                                       const osi3::SensorView& sensor_view)
{
    auto& ground_truth = sensor_view.global_ground_truth();

    ASSERT(ground_truth.moving_object_size() > 0 &&
           "TpmExample: received groundtruth does not contain any moving objects.");

    auto& original_object = ground_truth.moving_object(0);

    osi3::TrafficUpdate traffic_update;
    auto* moving_object = traffic_update.add_update();

    moving_object->CopyFrom(original_object);

    auto* base = moving_object->mutable_base();
    base->mutable_position()->set_x(123.45);

    num_spins_++;

    return traffic_update;
}

```

Note: in the TPM shared library, need to have a function aliased to `CreateTpm` using `boost:dll`

```c++
// file: traffic_participant_model_example.h
BOOST_DLL_ALIAS(TpmExample::Create, CreateTpm)

// file: traffic_participant_model_example.cpp
std::shared_ptr<ITrafficParticipantModel> TpmExample::Create(
    [[maybe_unused]] const std::map<std::string, std::string>& parameters)
{
    return std::make_shared<TpmExample>();
}
```


### Build the Plugin

1. To compile the example TPM plugin library, execute the Bazel build command, specifying the target `bazel build --config=gt_gen //Plugins/tpm_example:tpm_example.so`. Optionally, use `--config=gt_gen_release` for release mode.
2. Locate the compiled shared library file at `bazel-bin/Plugins/tpm_example/tpm_example.so`.
3. Copy the `.so` file to your desired directory.
4. Configure GT-Gen to locate the plugin by specifying its path in the **UserSettings**.ini file under `[UserDirectories]`.
    ```config
    [UserDirectories]
    Plugins = {/dir/path/to/plugin.so}

    ```

### Usage in OpenScenarios

To apply the plugin in **OpenSCENARIO** files, reference the `.so` library **name** in the `Controller` element of your `ScenarioObject`. Follow this by activating the controller for the scenario object through a `PrivateAction`.

```XML
<ScenarioObject name="TrafficVehicle">
     <ObjectController>
         <Controller name="traffic_participant_model_example">
             <Properties/>
         </Controller>
     </ObjectController>
 </ScenarioObject>
```

```XML
<PrivateAction>
    <ControllerAction>
        <ActivateControllerAction lateral="true" longitudinal="true"/>
    </ControllerAction>
</PrivateAction>
```

### Running the Simulation

Execute the simulation using `gtgen_cli`, ensuring the plugin `.so` file is within the appropriate directory. Verify the simulation's outcome by checking the trace, which should reflect the expected position `x` set by the TPM plugin.

```bash
cd ExampleData/TpmPlugin
# copy so file to the current dir
gtgen_cli -u UserSettings.ini  -s scenario/tpm_plugIn_example.xosc
```
