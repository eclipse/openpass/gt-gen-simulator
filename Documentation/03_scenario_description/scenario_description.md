## Scenario Description



By default GT-Gen supports the ASAM OpenSCENARIO® XML (ver 1.2.0) standard for scenarios (.xosc) This support comes through the integration of the [OpenScenarioEngine](https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine).

<hr>

### ASAM OpenSCENARIO® XML Standard

ASAM OpenSCENARIO is an internationally standardized open source scenario language in XML format, which enables for scenario exchange throughout e.g. different OEMs and test platforms (SiL, HiL). It also increases compatibility with other standard compliant tools in the toolchain (e.g. scenario editors, simulators, test tools). Since it has been developed for several years by many industry partners, it already offers a rich set of possible conditions and actions for scenario entities.


For an introduction to the standard and a guideline, how scenarios can be modeled and shall be interpreted and executed, please visit this link:

[User Guide 1.2 ](https://www.asam.net/index.php?eID=dumpFile&t=f&f=4908&token=ae9d9b44ab9257e817072a653b5d5e98ee0babf8#_changelog)( [User Guide 1.1]( 1.2 ))

When you want to know, which detailed features and possibilities ASAM OpenSCENARIO® XML offers while editing a scenario, please see this link:

[UML Model 1.2](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.2.0_Model_Documentation/modelDocumentation/) ([UML Model 1.1](https://www.asam.net/static_downloads/modelDocumentation/index.html))

When editing a scenario manually, it's recommended to validate the file against the schema (xmllint) to avoid syntax errors. The schema is available here:
[XSD Schema 1.2](https://asc.bmwgroup.net/wiki/download/attachments/630726564/OpenSCENARIO_StrictValidation_1_2.xsd?version=1&modificationDate=1652768759000&api=v2) [(XSD schema 1.1](https://github.com/asam-oss/OSC-ALKS-scenarios/blob/master/.github/workflows/OpenSCENARIO_StrictValidation_1_1.xsd))


> ❕ Please note that adherence to the XSD schema is enforced in GT-Gen.


<hr>

### Ego/Host vehicle

The ASAM OpenSCENARIO® XML standard would not require the definition of an ego/host vehicle in the scenario because the standard can also be used for describing generic traffic simulation scenarios. GT-Gen on the other hand is designed to simulate the environment for an ego vehicle containing a system under test. Therefore GT-Gen requires the definition of exactly one ego/host vehicle in the scenario. Further specifics when modelling ASAM OpenSCENARIO® XML scenarios for GT-Gen are listed below.

#### Naming and ID
GT-Gen uses the convention that the entity with the name "Ego" or "Host" is considered the ego vehicle. So every ASAM OpenSCENARIO® XML scenario which should be executable in GT-Gen must have exactly one entity with the name "Ego" or "Host" defined.The ego will always receive the ID 0 in ground truth.

#### Actions
In "ExternalVehicle" configuration the ego vehicle's motion is not controlled by GT-Gen but by an external controller containing an integrated system under test. Therefore not all scenario actions which can already be executed by the GT-Gen internal controllers (e.g. for the traffic vehicles) are supported yet for the ego vehicle with "ExternalVehicle" control. Also since an external controller may use a realistic vehicle dynamics model with dynamic limitations, movement related actions may not be executed exactly like described in the scenario (e.g. a SpeedAction with unlimited jerk).


<hr>

### Other Entities
#### Naming and IDs
If the name of another entity besides the host vehicle can be interpreted as an unsigned integer number in the range [1;1000] (e.g. "5"), it will also get the corresponding ID in the ground truth. Integers 0 (reserved for host) and >1000 are not allowed as names. If the name does not specify an integer ID, the entity will get an arbitrary ID in the ground truth (>1000). It is not allowed to assign the same name or ID to two entities.

#### Internal ego vehicle state

In ASAM OpenSCENARIO® XML scenarios only the environment of the ego vehicle (i.e. traffic, weather...) shall be modelled in relation to the ego vehicle's position. So the ego vehicle needs to be included as an entity with its dimensions for reference and movement simulation while the ADAS/AD function is not active, but the internal state of the ego vehicle is not part of the scenario.

<hr>

### Traffic agents

For modelling scenarios with intelligent traffic agents rather than prescribed traffic behavior, ASAM OpenSCENARIO® XML offers the `Controller` feature.

Loading a particular plugin is specified by the name of the Controller in a ScenarioObject node, for example::

<Entities>
  <ScenarioObject name="TrafficVehicle">
    <CatalogReference catalogName="VehicleCatalog" entryName="car_ego"></CatalogReference>
    <ObjectController>
      <Controller name="traffic_participant_model">
        <Properties>
          <Property name="velocity" value="10"/>
          <Property name="behaviour_file_path" value="Default.json"/>
        </Properties>
      </Controller>
    </ObjectController>
  </ScenarioObject>
</Entities>


You can improve the reusability of controllers for different scenarios by using a controller catalog and referencing the controller from there using a `CatalogReference` instead of an inline `Controller` definition.

The current implementation has been updated to require that any controllers assigned to the entity via GT-Gen must also be activated, ensuring compliance with the ASAM OpenSCENARIO® XML standard.

>⚠️ To-be-reconstructed:
>
> Collected infor:
The plugin libraries have to be compiled as .so shared libraries and available in the Plugins directory.
>
> A new directory for the plugins exists at Simulator/Plugins by default. This can be customized in a similar manner to the maps and scenarios in the UserSettings.ini under [UserDirectories].
>
> External controllers now require the name "Ego", "Host", or "ExternalHost". This changes the name of the controller from "ALKSController" to "ExternalHost".
>
> https://gitlab.eclipse.org/eclipse/openpass/osi-traffic-participant
> Update readme


<hr>

### Traffic lights

Dynamic behavior, e.g. switching traffic lights is supported in **GT-Gen** if the traffic lights are defined as `MiscObject` entities in a scenario or catalog.

Here is an example with the whole light box, consisting of several light bulbs, as one traffic light for more convenient handling.

```xml
<MiscObject miscObjectCategory="none" mass="0" name="traffic_light_vertical_red_yellow">
    <BoundingBox>
        <Center x="0.0" y="0.0" z="0.5"/>
        <Dimensions width="1.0" length="1.0" height="1.0"/>
    </BoundingBox>
    <Properties>
        <Property name="object_type" value="traffic_light"/>
        <Property name="type" value="1.000.009"/>
        <Property name="sub_type" value="10"/>
        <Property name="initial_state" value="off,off"/>
        <Property name="mount_height" value="2.2"/>
    </Properties>
</MiscObject>
```

The property object_type is mandatory so the MiscObject is recognized as a traffic light. type and sub_type must be taken from the list of supported traffic light identifiers.
initial_state is optional and contains the individual light bulb modes separated by a comma from top to bottom.
The number of modes must match the number of light bulbs. mount_height is optional and is added to the z-value of the MiscObject position.

In OpenSCENARIO you can switch traffic light states either continuously with so called TrafficSignalControllers or only once with a TrafficSignalStateAction. Currently TrafficSignalStateAction is supported. The syntax for the state is the same as for the initial_state. In the example below you can see, how the state in a TrafficSignalStateAction shall be written for e.g. a red light:

```xml
<GlobalAction>
    <InfrastructureAction>
        <TrafficSignalAction>
            <TrafficSignalStateAction name="TrafficSignal1" state="constant,off,off"/>
        </TrafficSignalAction>
    </InfrastructureAction>
</GlobalAction>
```

<hr>

### Traffic swarm

GT-Gen supports automatic traffic generation through the [OpenSCENARIO TrafficSwarmAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TrafficSwarmAction.html).

The current version only allows for a circular spawning area centered around the central entity. Therefore, the '`innerRadius`', '`semiMajorAxis`' and '`offset`' parameters are not supported. We are currently using the '`semiMinorAxis`' to set the radius of the spawning area.

List of supported parameters:

- `innerRadius` : **Not yet supported.**
- `numberOfVehicles` : defines the amount of vehicles the action should try to spawn at any given time of the simulation. If the amount is reached, no new vehicle will be spawned until another traffic vehicle is despawned. If the amount is not reached but no spawning positions are available, new vehicles will be spawned once new positions are made available.
- `offset` : **Not yet supported.**
- `semiMajorAxis` : defines the major radius of the spawning area's ellipse. **Not yet supported.**
- `semiMinorAxis` : is used to set the radius of the spawning area (circular).
- `velocity` : defines the velocity of the spawned vehicles. If this value is larger than the central entity's velocity, the vehicles will be spawned behind the central entity, not to be instantly despawned because of leaving the spawning area. Else, if this value is smaller than the central entity's velocity, the vehicles will be spawned in front of the central entity, not to be instantly despawned because of leaving the spawning area.
- `CentralObject` : name of the central entity which will be at the center of the spawning area.
- `TrafficDefinition` : defines weighted distributions to describe the spawned vehicles types (car, motorbike, truck, ...) and their associated controllers.

```XML
<GlobalAction>
    <TrafficAction trafficName="SwarmTraffic">
        <TrafficSwarmAction innerRadius="5.0" numberOfVehicles="10" offset="0.0" semiMajorAxis="20.0" semiMinorAxis="10.0" velocity="10.0">
            <CentralObject entityRef="Ego" />
            <TrafficDefinition name="TrafficDefinition">
                <VehicleCategoryDistribution>
                    <VehicleCategoryDistributionEntry category="car" weight="1.0" />
                </VehicleCategoryDistribution>
                <ControllerDistribution>
                    <ControllerDistributionEntry weight="1.0">
                        <CatalogReference catalogName="ControllerCatalog" entryName="traffic_participant_model" />
                    </ControllerDistributionEntry>
                </ControllerDistribution>
            </TrafficDefinition>
        </TrafficSwarmAction>
    </TrafficAction>
</GlobalAction>
```

<hr>

### Routing behavior


If no routing actions are assigned to movable entities, for example for vehicles and pedestrians, not for MiscObjects, then ASAM OpenSCENARIO® XML currently defines a default routing behavior that follows the lane.
When intersections are reached with ASAM OpenSCENARIO® XML.2 no consistent behavior is defined regarding which option to choose. Therefore in GT-Gen the longest route of the entity from the current position along the loaded road network is currently planned by default.
This is applied to all movable entities controlled by GT-Gen for the traffic and ego/host vehicle.


#### Vehicles
If you create logical scenarios on road networks with many intersections we recommend to use an AssignRouteAction in the init section for each vehicle which should follow a lane.
This avoids unspecified behavior at intersections.

On the other hand if you use a road network without intersections, a straight lane for example, AssignRouteActions
are not needed.
Also unless vehicles should only follow a straight line, it is recommended to use routes for a more realistic movement.
Following trajectories is following a straight line between the vertices and a linear interpolation of the orientation between the vertices.
Following routes is following a spline aligned to the lane centerline. Orientation here is aligned with to the spline.


#### Pedestrians

Pedestrians are in general able to behave like vehicles and also follow lanes, but since they often do not follow lanes for example, when they should cross a lane, we recommend to use `FollowTrajectoryActions`.


<hr>

### Pylons (traffic cones)

A pylon should be added using MiscObjectCatalog.xosc. In this catalog, by default the pylon is given a zero height and therefore will be on the ground in a simulation.

<hr>

### Catalogs
ASAM OpenSCENARIO supports the concept of catalogs to increase reusability of scenario elements. The general concept is described in the OpenSCENARIO User Guide.

*Which catalogs should be changed?*
> The vehicle and pedestrian catalogs contain the models currently supported by GT-Gen.The MiscObject catalog on the other hand can be extended with variables, such as obstacles (Pylon...). New catalogs can also be added. For the different types of catalogs, check the UML model of OpenSCENARIO. All catalogs of the same type (e.g. vehicle) need to be in the same folder.



#### Parameter Assignments
If an element in a catalog does not exactly meet your requirements, but for example only one attribute needs to be changed only for your logical scenario, you can parametrize this attribute instead of copying the whole catalog element

```xml
<MiscObject miscObjectCategory="none" mass="0" name="traffic_light_vertical_red_yellow">
    <ParameterDeclarations>
        <ParameterDeclaration name="mount_height" parameterType="string" value="2.2"/>
    </ParameterDeclarations>
    <BoundingBox>
        <Center x="0.0" y="0.0" z="0.5"/>
        <Dimensions width="1.0" length="1.0" height="1.0"/>
    </BoundingBox>
    <Properties>
        <Property name="object_type" value="traffic_light"/>
        <Property name="type" value="1.000.009"/>
        <Property name="sub_type" value="10"/>
        <Property name="initial_state" value="off,off"/>
        <Property name="mount_height" value="$mount_height"/>
    </Properties>
</MiscObject>
```

In the scenario you can then assign a different value to the parameter than the default value from the catalog:

```xml
<EntityObject>
    <CatalogReference catalogName="MiscObjectCatalog" entryName="traffic_light_vertical_red_yellow">
    <ParameterAssignments>
        <ParameterAssignment parameterRef="mount_height" value="5.0"/>
    </ParameterAssignments>
    </CatalogReference>
</EntityObject>
```

<hr>

###	Troubleshooting

Find solutions to common problems that users may encounter during their GT-Gen experience, enhancing the overall usability.
When your modeled scenario/map won't run with GT-Gen, check the console to see if there were any scenario/map validation errors.
If there are, then your logical scenario/map is probably not schema compliant or has a semantic error.

Usually for these kinds of errors GT-Gen gives a detailed output of what the error is with an exact location in the assets file of where the error occurred.

<hr>

### Scenario Editor
GT-Gen does not provide any graphical based scenario Editor. However, there are some open-source editors that can be used, e.g.
- [Blender Driving Scenario Creator add-on](https://github.com/johschmitz/blender-driving-scenario-creator)
- [Open Scenario Editor](https://github.com/ebadi/OpenScenarioEditor)
