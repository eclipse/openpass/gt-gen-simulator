
### Supported OSC-ALKS-scenarios

GT-Gen's current release does not fully support all ASAM OpenSCENARIO features (see [Supported OpenScenario Features](supported_osc_features.md)), impacting its ability to directly utilize several Automated Lane Keeping System (ALKS) scenarios as provided on the [ALKS GitHub-page](https://github.com/asam-oss/OSC-ALKS-scenarios). The specific unsupported features include:


- `LaneOffsetAction`
- `coordinateSystem`=`lane` in `TimeHeadwayCondition`
- `dynamicsShape`= `sinusoidal` in `LaneChangeActionDynamics`
- `RelativeLanePosition` = `ds`
- `StoryboardElementStateCondition`


To mitigate these limitations, GT-Gen offers adapted scenarios within the "ALKS_Scenarios" directory. These modifications ensure that the scenarios deliver equivalent functional behaviors and remain compatible with GT-Gen's existing features.


- [x] ALKS_Scenario_4.1_1_FreeDriving_TEMPLATE.xosc
- [ ] ALKS_Scenario_4.1_2_SwervingLeadVehicle_TEMPLATE.xosc
- [x] ALKS_Scenario_4.1_3_SideVehicle_TEMPLATE.xosc
- [x] ALKS_Scenario_4.2_1_FullyBlockingTarget_TEMPLATE.xosc
- [x] ALKS_Scenario_4.2_2_PartiallyBlockingTarget_TEMPLATE.xosc
- [x] ALKS_Scenario_4.2_3_CrossingPedestrian_TEMPLATE.xosc
- [x] ALKS_Scenario_4.2_4_MultipleBlockingTargets_TEMPLATE.xosc
- [x] ALKS_Scenario_4.3_1_FollowLeadVehicleComfortable_TEMPLATE.xosc
- [x] ALKS_Scenario_4.3_2_FollowLeadVehicleEmergencyBrake_TEMPLATE.xosc
- [x] ALKS_Scenario_4.4_1_CutInNoCollision_TEMPLATE.xosc
- [x] ALKS_Scenario_4.4_2_CutInUnavoidableCollision_TEMPLATE.xosc
- [x] ALKS_Scenario_4.5_1_CutOutFullyBlocking_TEMPLATE.xosc
- [x] ALKS_Scenario_4.5_2_CutOutMultipleBlockingTargets_TEMPLATE.xosc
- [x] ALKS_Scenario_4.6_1_ForwardDetectionRange_TEMPLATE.xosc
- [ ] ALKS_Scenario_4.6_2_LateralDetectionRange_TEMPLATE.xosc
