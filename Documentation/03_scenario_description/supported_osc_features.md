﻿## GT·GEN Supports of ASAM OpenSCENARIO® XML



Currently GT-Gen-Simulator can execute the following features of OpenSCENARIO XML (version 1.3.0) scenarios (scenarios using version 1.2.0 and earlier can also be executed):


### Features

```
Feature
│
├── ParameterDeclaration
│   └── Constraints ··························· ✅
│
├── Expressions
│   ├── Arithmetic calculations ··············· ✅
│   └── Logical expressions  ·················· ✅
│
├── Catalogs
│   └── ParameterAssignments ·················· ✅
│
├── RoadNetwork
│   ├── LogicFile ····························· ✅
│   ├── SceneGraphFile ························ ❌
│   ├── TrafficSignals ························ ❌
│   └── UsedArea ······························ ✅ (only with GeoPositions)
│
├── Entities
│   │
│   ├── ScenarioObject
│   │   ├── EntityObject
│   │   │   ├── CatalogReference ·············· ✅
│   │   │   ├── Vehicle
│   │   │   │   ├── Name  ····················· ✅
│   │   │   │   ├── Model3d  ·················· ✅
│   │   │   │   ├── Mass  ····················· ✅
│   │   │   │   ├── ParameterDeclaration  ····· ✅
│   │   │   │   ├── Performance  ·············· ✅
│   │   │   │   ├── BoundingBox  ·············· ✅
│   │   │   │   ├── Axles  ···················· ✅
│   │   │   │   ├── VehicleCategory  ·········· ✅
│   │   │   │   └── Properties  ··············· ✅
│   │   │   ├── Pedestrian
│   │   │   │   ├── Name ······················ ✅
│   │   │   │   ├── Model3d ··················· ✅
│   │   │   │   ├── Mass ······················ ❌
│   │   │   │   ├── ParameterDeclaration ······ ✅
│   │   │   │   ├── PedestrianCategory ········ ✅
│   │   │   │   ├── BoundingBox ··············· ✅
│   │   │   │   └── Properties ················ ✅
│   │   │   ├── MiscObject
│   │   │   │   ├── Model3d ··················· ✅
│   │   │   │   ├── Name ······················ ✅
│   │   │   │   ├── Mass ······················ ❌
│   │   │   │   ├── ParameterDeclaration ······ ✅
│   │   │   │   ├── PedestrianCategory ········ ✅
│   │   │   │   ├── BoundingBox ··············· ✅
│   │   │   │   └── Properties ················ ✅ (* traffic lights and signals)
│   │   │   └── ExternalObjectReference ······· ❌
│   │   │
│   │   └── ObjectController
│   │       ├── CatalogReference ·············· ✅
│   │       └── Controller ···················· ✅
|   |
│   └── EntitySelection ······················· ❌
│
├── Storyboard
│   ├── Init
│   │   └── Actions ··························· ✅
│   ├── Story
│   │   ├── ManeuverGroup ····················· ✅
│   │   ├── StartTrigger ······················ ✅
│   │   └── StopTrigger ······················· ✅
│   └── StopTrigger ··························· ✅
│
├── ManeuverGroup
│   ├── maximumExecutionCount ················· ❌
│   ├── Actors
│   │   ├── entityRefs ························ ✅
│   │   └── selectTriggeringEntities ·········· ✅
│   ├── CatalogReference ······················ ❌
│   └── Maneuver
│       └── Event
│           ├── maximumExecutionCount ········· ❌
│           ├── priority ······················ ✅
│           ├── Actions ······················· ✅
│           └── StartTrigger ·················· ✅
└── Trigger
    └── ConditionGroup
        └── Condition ························· ✅
```


### Conditions

```
Conditions
│
├── ConditionEdge
│   ├── rising ································ ✅
│   ├── falling ······························· ✅
│   ├── risingOrFalling ······················· ✅
│   └── none ·································· ✅
│
├── Delay ····································· ✅
│
├── ByEntityCondition
│   ├── TriggeringEntities
│   │   └── TriggeringEntitiesRule
│   │       ├── any ··························· ✅
│   │       └── all ··························· ✅
│   └── EntityCondition
│       ├── EndOfRoadCondition ················ ❌
│       ├── CollisionCondition ················ ❌
│       ├── OffroadCondition ·················· ❌
│       ├── TimeHeadwayCondition ·············· ✅
│       │   ├── coordinateSystem ·············· ✅
│       │   │   ├── entity ···················· ✅
│       │   │   ├── lane ······················ ✅
│       │   │   ├── road ······················ ❌
│       │   │   └──trajectory ················· ❌
│       │   ├── entityRef ····················· ✅
│       │   ├── freespace ····················· ✅
│       │   └── relativeDistanceType ·········· ✅
│       │       ├── lateral ··················· ❌
│       │       ├── longitudinal ·············· ✅
│       │       ├── euclidianDistance ········· ❌
│       │       └── cartesianDistance ········· ❌ (Enumeration literal cartesianDistance deprecated. With version 1.1. Use euclideanDistance.)
│       ├── TimeToCollisionCondition ·········· ❌
│       ├── AccelerationCondition ············· ❌
│       ├── StandStillCondition ··············· ❌
│       ├── SpeedCondition ···················· ❌
│       ├── RelativeSpeedCondition ············ ✅
│       │    └──  direction ··················· ❌ (* only length of velocity vector is compared)
│       ├── TraveledDistanceCondition ········· ❌
│       ├── ReachPositionCondition ············ ✅
│       ├── DistanceCondition ················· ✅
│       └── RelativeDistanceCondition ········· ✅
│           ├── coordinateSystem ·············· ✅
│           │   ├── entity ···················· ✅
│           │   ├── lane ······················ ❌
│           │   ├── road ······················ ❌
│           │   ├── world ····················· ❌
│           │   └──trajectory ················· ❌
│           ├── entityRef ····················· ✅
│           ├── freespace ····················· ✅
│           └── relativeDistanceType ·········· ✅
│               ├── lateral ··················· ❌
│               ├── longitudinal ·············· ✅
│               └── euclidianDistance ········· ❌
│
├── ByValueCondition
│   ├── ParameterCondition ···················· ❌
│   ├── TimeOfDayCondition ···················· ❌
│   ├── SimulationTimeCondition ··············· ✅
│   ├── StoryBoardElementStateCondition ······· ❌
│   ├── UserDefinedValueCondition ············· ✅
│   ├── TrafficSignalCondition ················ ❌
│   └── TrafficSignalControllerCondition ······ ❌
│
└──  Rule (* Used in many conditions)
    ├── equalTo ······························· ✅
    ├── greaterThan ··························· ✅
    ├── lessThan ······························ ✅
    ├── greaterOrEqual ························ ✅
    ├── lessOrEqual ··························· ✅
    └── notEqualTo ···························· ✅

```

### PrivateActions

```
PrivateActions
│
├── LongitudinalAction ························ ✅
│   ├── SpeedAction ··························· ✅
│   │   ├── Dynamics ·························· ✅
│   │   │   ├── Shape ························· ✅
│   │   │   │   ├── linear ···················· ✅
│   │   │   │   ├── cubic ····················· ⚠️ (Cubic if selected will change to linear)
│   │   │   │   ├── sinusoidal ················ ❌
│   │   │   │   └── step ······················ ✅
│   │   │   ├── Dimension ····················· ✅
│   │   │   │   ├── rate ······················ ✅
│   │   │   │   ├── time ······················ ✅
│   │   │   │   └── distance ·················· ✅
│   │   │   └── followingMode ················· ✅
│   │   │       ├── position ·················· ✅
│   │   │       └── follow ···················· ✅
│   │   └── Target ···························· ✅
│   │       ├── Relative ······················ ✅
│   │       │   ├── continuous ················ ❌
│   │       │   └── SpeedTargetValueType ······ ✅
│   │       └── Absolute ······················ ✅
│   │
│   ├── LongitudinalDistanceAction ············ ✅
│   │   ├── continuous ························ ✅
│   │   │   ├── true ·························· ❌
│   │   │   └── false ························· ✅
│   │   ├── freespace ························· ✅
│   │   ├── distance ·························· ✅
│   │   ├── timeGap ··························· ✅
│   │   ├── DynamicConstraints ················ ❌
│   │   └── displacement ······················ ✅
│   │   │   ├── any ··························· ❌
│   │   │   ├── trailing ······················ ✅
│   │   │   └── leading ······················· ✅
│   │   └── CoordinateSystem ·················· ✅
│   │       ├── entity ························ ❌
│   │       ├── lane ·························· ✅
│   │       ├── road ·························· ❌
│   │       └── trajectory ···················· ❌
│   │
│   └── SpeedProfileAction ···················· ❌
│
├── LateralAction ····························· ✅
│   ├── LaneChangeAction ······················ ✅
│   │   ├── TargetLaneOffset ·················· ✅
│   │   ├── LaneChangeDynamics ················ ✅
│   │   │   ├── Shape ························· ✅
│   │   │   │   ├── linear ···················· ✅
│   │   │   │   ├── cubic ····················· ✅
│   │   │   │   ├── sinusoidal ················ ✅
│   │   │   │   └── step ······················ ❌
│   │   │   └── Dimension ····················· ✅
│   │   │       ├── time ······················ ✅
│   │   │       ├── distance ·················· ✅
│   │   │       └── rate ······················ ✅
│   │   └──LaneChangeTarget ··················· ✅
│   │       ├── RelativeTargetLane ············ ✅ (* Limitation if referenced entity is not the actor, then it must be on
│   │       │                                       a neighboring lane to the actor i.e. no successor/predecessor)
│   │       └── AbsoluteTargetLane ············ ✅ (* Local lane ID of the road (as per ODR convention) ODR: starting from
│   │                                               0 at the center lane and positive to the left )
│   ├── LaneOffsetAction ······················ ✅
│   │   ├── continuous ························ ❌
│   │   ├── LaneOffsetActionDynamics ·········· ❌
│   │   │   ├── DanymicShape ·················· ❌
│   │   │   │   ├── linear ···················· ❌
│   │   │   │   ├── cubic ····················· ❌
│   │   │   │   ├── sinusoidal ················ ❌
│   │   │   │   └── step ······················ ❌
│   │   │   └── maxLateralAcc ················· ❌
│   │   └── LaneOffsetTarget ·················· ✅
│   │       ├── RelativeTargetLaneOffset ······ ❌
│   │       │   ├── entityRef ················· ❌
│   │       │   └── value ····················· ❌
│   │       └── AbsoluteTargetLaneOffset ······ ✅
│   │           └── value ····················· ✅
│   │
│   └── LateralDistanceAction ················· ✅
│       ├── continuous ························ ✅
│       │   ├── true ·························· ❌
│       │   └── false ························· ✅
│       ├── coordinateSystem ·················· ✅
│       │   ├── entity ························ ❌
│       │   ├── lane ·························· ✅
│       │   ├── road ·························· ❌
│       │   └──trajectory ····················· ❌
│       ├── displacement ······················ ✅
│       │   ├── any ··························· ✅
│       │   ├── leftToReferencedEntity ········ ✅
│       │   └── rightToReferencedEntity ······· ✅
│       ├── distance ·························· ✅
│       ├── entityRef ························· ✅
│       ├── freespace ························· ✅
│       │   ├── true ·························· ✅
│       │   └── false ························· ✅
│       └── dynamicConstraints ················ ❌
│           ├── maxAcceleration ··············· ❌
│           ├── maxAccelerationRate ··········· ❌
│           ├── maxDeceleration ··············· ❌
│           ├── maxDecelerationRate ··········· ❌
│           └── maxSpeed ······················ ❌
│
├── VisibilityAction··························· ✅
│   ├── SensorReferenceSet ···················· ✅
│   ├── graphics ······························ ❌
│   ├── sensors ······························· ✅
│   └── traffic ······························· ✅
│
│
├── ControllerAction··························· ✅
│   ├── AssignControllerAction ················ ✅
│   ├── OverrideControllerValueAction ········· ❌
│   └── ActivateControllerAction ·············· ✅
│
├── TeleportAction ···························· ✅
│   ├── WorldPosition ························· ✅
│   ├── RelativeWorldPosition ················· ❌
│   ├── RelativeObjectPosition ················ ❌
│   ├── RoadPosition ·························· ✅
│   ├── RelativeRoadPosition ·················· ❌
│   ├── LanePosition ·························· ✅
│   ├── RelativeLanePosition ·················· ✅ (* only dsLane not ds)
│   ├── RoutePosition ························· ❌
│   ├── GeoPosition ··························· ✅ (* also latitudeDeg and longitudeDeg)
│   └── TrajectoryPosition ···················· ❌
│
├── RoutingAction
│   ├── AssignRouteAction ····················· ✅
│   │   ├── Route ····························· ✅
│   │   │   ├── closed ························ ✅
│   │   │   │   ├── true ······················ ❌
│   │   │   │   └── false ····················· ✅
│   │   │   ├── name ·························· ✅
│   │   │   ├── ParameterDeclaration ·········· ✅
│   │   │   └── Waypoint ······················ ✅
│   │   │       ├── RouteStrategy ············· ❌ (* 'shortest' route assumed)
│   │   │       └── Position ·················· ✅ (* See TeleportAction Status position types)
│   │   └── CatalogReference ·················· ✅ (* CatalogElement·>Route)
│   │
│   ├── FollowTrajectoryAction ················ ✅
│   │   ├── Trajectory ························ ❌ (* Deprecated)
│   │   ├── CatalogReference ·················· ✅ (* Deprecated)
│   │   ├── TimeReference ····················· ✅
│   │   ├── TrajectoryFollowingMode ··········· ❌ (* 'position' assumed)
│   │   └── TrajectoryRef ····················· ✅
│   │       ├── Trajectory ···················· ✅
│   │       │   ├── parameterDeclarations ····· ✅
│   │       │   ├── Shape ····················· ✅ (* Only 'polyline' Status)
│   │       │   └── Closed ···················· ❌ (* 'false' assumed)
│   │       └── Trajectory ···················· ✅
│   │
│   └── AcquirePositionAction ················· ✅
│
├── SynchronizeAction·························· ❌
├── AppearanceAction··························· ❌
└── ActivateControllerAction (deprecated)
```

### UserDefinedActions

```
UserDefinedActions
│
└──   CustomCommandAction ····················· ✅ (* but no specific commands defined)
```


### GlobalActions
```
GlobalActions
│
├── EnvironmentAction ························· ❌
│
├── EntityAction ······························ ✅
│   ├── DeleteEntityAction ···················· ✅
│   └── AddEntityAction ······················· ❌
│
├── ParameterAction ··························· ❌
│
├── InfrastructureAction ······················ ✅
│   ├── TrafficSignalStateAction ·············· ✅
│   └── TrafficSignalControllerAction ········· ❌
│
└── TrafficAction ····························· ❌
    ├── TrafficSourceAction ··················· ❌
    ├── TrafficSinkAction ····················· ❌
    ├── TrafficSignalStateAction ·············· ❌
    ├── TrafficSwarmAction ···················· ✅
    │   ├── innerRadius ······················· ❌ (* Only the semiMajorAxis is used to define a circular spawning area centered around the central entity.)
    │   ├── numberOfVehicles ·················· ✅
    │   ├── offset ···························· ❌ (* Only the semiMajorAxis is used to define a circular spawning area centered around the central entity.)
    │   ├── semiMajorAxis ····················· ✅ (* Only works for despawning entities in an ellipse area, not for spawning.)
    │   ├── semiMinorAxis ····················· ✅
    │   ├── velocity ·························· ✅
    │   ├── centralObject ····················· ✅
    │   └── trafficDefinition ················· ✅ (* Only the the small car vehicle class is currently Status.)
    └── TrafficStopAction ····················· ❌

```
