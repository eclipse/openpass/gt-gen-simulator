/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_CLI_CLI_H
#define GTGEN_SIMULATOR_CLI_CLI_H

#include "Cli/command_line_parser.h"
#include "Core/Export/simulation_parameters.h"
#include "Simulator/Utils/UserData/user_data_manager.h"

#include <chrono>
#include <memory>

namespace gtgen::simulator::cli
{

/// @brief This entity is in charge of evaluating the command line arguments, and deciding if the gtgen simulation shall
/// be started up or not.
class Cli
{
  public:
    /// @brief Code indicating if the simulation run was terminated or ended after the specified duration
    enum class RunCode
    {
        kSimulationTimeOver = 0,
        kSimulationTerminated = 1
    };

    explicit Cli(const CommandLineParser& cmd_line_parser);

    /// @brief Decides which of the following functions is executed depending on the parsed parameters
    int Execute();

    /// @brief Creates the simulator and runs it
    RunCode RunSimulation();

  protected:
    virtual void InstallUserData() const;

    /// @brief Validates a passed in map. Only OpenDRIVE maps can be validated.
    ///
    /// @return Zero on success, error-code otherwise:
    ///          1 = Map contains warnings
    ///          2 = Map contains errors
    ///          3 = Unsupported map format
    virtual int ValidateMap() const;

    /// @brief Validates a passed in scenario.
    ///
    /// @return Zero on success, otherwise the number of errors encountered.
    int ValidateScenario() const;

    const CommandLineParser& cmd_line_parser_;
    gtgen::core::SimulationParameters simulation_parameters_;
    std::unique_ptr<simulation::user_data::UserDataManager> user_data_manager_;
};

}  // namespace gtgen::simulator::cli

#endif  // GTGEN_SIMULATOR_CLI_CLI_H
