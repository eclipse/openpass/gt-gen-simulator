/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Cli/sleep_timer.h"

namespace gtgen::simulator::cli
{

SleepTimer::SleepTimer(std::chrono::milliseconds step_size, double time_scale)
    : step_size_ms_{step_size}, time_scale_{time_scale}
{
}

std::chrono::milliseconds SleepTimer::ComputeSleepTime(
    const std::chrono::time_point<std::chrono::steady_clock>& before,
    const std::chrono::time_point<std::chrono::steady_clock>& after) const
{
    auto run_once_duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(after - before);
    auto time_to_sleep = (step_size_ms_ * (1 / time_scale_)) - run_once_duration_ms;

    return std::chrono::duration_cast<std::chrono::milliseconds>(time_to_sleep);
}

}  // namespace gtgen::simulator::cli
