/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_CLI_VERSION_H
#define GTGEN_SIMULATOR_CLI_VERSION_H

#include "Core/Service/Utility/version.h"

namespace gtgen::simulator::cli
{

/// @brief This is the current GtGen cli version. Changes must be applied in the packaging BUILD file accordingly.
constexpr gtgen::core::Version gtgen_cli_version{GTGEN_CLI_VERSION_MAJOR,
                                                 GTGEN_CLI_VERSION_MINOR,
                                                 GTGEN_CLI_VERSION_PATCH};

}  // namespace gtgen::simulator::cli

#endif  // GTGEN_SIMULATOR_CLI_VERSION_H
