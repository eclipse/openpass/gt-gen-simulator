/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Cli/cli.h"
#include "Cli/command_line_parser.h"

#include <iostream>

/// @brief The main entrance point for the GtGen CLI
int main(int argc, const char** argv)
{
    gtgen::simulator::cli::CommandLineParser cmd_line_parser{};

    if (!cmd_line_parser.Parse(argc, argv))
    {
        // Logging framework not yet setup
        std::cerr << cmd_line_parser.GetParserMessage() << std::endl;
        return 1;
    }

    gtgen::simulator::cli::Cli cli{cmd_line_parser};
    return cli.Execute();
}
