# Dummy ALKSController for ALKS Scenarios

The  **Dummy ALKSController** demonstrates a simple and minimal functionality to help users understand how they can implement and test a real **ALKSController** using the **Gt-Gen** simulator.

The provided controller is built into the shared library **`ALKSController.so`** (a symbolic link to **`tpm_example.so`**) and is designed to move the vehicle along the lane at a fixed speed.  Although it is a simple reference, users can easily replace this with their own **fully functional ALKS controllers** or modify the behavior within their simulation.

---

## **Installation**

Upon installation of **gtgen-simulator**, the dummy **ALKSController.so** will be placed in the following location by default:

```
~/GTGEN_DATA/Plugins/ALKSController.so
```


---

## **Functionality of the Dummy ALKSController**

The dummy **ALKSController** is a basic implementation with the following features:

- **Fixed Speed:** The vehicle maintains a speed of **30 km/h**.
- **No Complex Reactions:** It does not keep lane, or react to other vehicles, obstacles, or environmental changes.

This dummy controller serves only as a **reference** and is intended to be replaced by custom, fully functional controllers.

---

## **How to Use the Dummy ALKSController**

### **Step 1: Assign the Controller in the OpenScenario File**
The OpenScenario file (`.xosc`) used in the simulation should include a controller assignment section. The default controller name is **`ALKSController`**, and it should be referenced like this:

```xml
<Controller name="ALKSController"/>
```

If you want to use a different controller name, you will need to **modify the OpenScenario file** to match the correct controller reference.

---

### **Step 2: Configuring the UserSettings File**

By default, **gtgen-simulator** will look for **`ALKSController.so`** in:

```
~/GTGEN_DATA/Plugins/
```

If the controller is not in the default location, or if you have a custom implementation you want to test, you can specify the **UserDirectories:Plugins** using the **UserSettings configuration file**.

Modify the **UserSettings** file to specify the location of the controller if it differs from the default. The configuration file should contain an entry similar to:

```
[UserDirectories]
Plugins=/path/to/your/custom
```

For example, if you have a custom implementation located at `/home/user/custom_plugins/`, the entry would be:

```
[UserDirectories]
Plugins=/home/user/custom_plugins
```

---

## **Developing a Custom ALKSController**

Please follow the [GT-Gen TPM Plugin Tutorial: Loading and Implementing TPM in Simulations](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/05_tutorials/tpm_plugin.md?ref_type=heads) to developer your TPM model.



---

## **Testing and Running the Scenario**

1. **Create or Select an ALKS Test Scenario:** You can use one of the ALKS scenarios from the **[openMSL ALKS repository](https://github.com/openMSL/sl-3-1-osc-alks-scenarios)** or create your own.
2. **Verify the Controller Assignment:** Ensure that the OpenScenario file references the correct controller (default: **ALKSController**).
3. **Run the Simulation:** Launch the scenario, and observe how the dummy **ALKSController** behaves.


## **Contact**

For further assistance, please refer to the [**documentation**](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/SUMMARY.md?ref_type=heads) or contact the development team.
