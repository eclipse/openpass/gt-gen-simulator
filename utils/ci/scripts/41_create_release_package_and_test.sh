#!/usr/bin/env bash

# *******************************************************************************
# Copyright (C) 2025, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

set -euo pipefail

source "$(dirname "${BASH_SOURCE[0]}")/helper_functions/bash_helper.sh"

# Define directory paths
ROOT_DIR="$(dirname "$(readlink -f "$0")")"
BASEDIR=$(realpath "${ROOT_DIR}/../../../..")
CACHEDIR=$(realpath "/home/jenkins/cache/gtgen_simulator")

SIMULATOR_INSTALL_DIR="${BASEDIR}/gtgen-bin"
SIMULATOR_HOME="${SIMULATOR_INSTALL_DIR}/opt/gtgen_core"
CLI_HOME="${SIMULATOR_INSTALL_DIR}/opt/gtgen_cli"

# Bazel cache setup
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Update environment paths
export PATH="${CLI_HOME}:${PATH}"
export LD_LIBRARY_PATH="${SIMULATOR_HOME}/lib:${LD_LIBRARY_PATH:-}"

function cleanup_artifacts()
{
    log::run "Cleaning up artifacts..."
    mkdir -p "${BASEDIR}/artifacts"
    rm -f "${BASEDIR}/artifacts"/gtgen-simulator_*.deb
    rm -f "${BASEDIR}/artifacts"/gtgen-cli_*.deb

    rm -f bazel-bin/Packaging/gtgen-simulator_*.deb
    rm -f bazel-bin/Packaging/gtgen-cli_*.deb
}

function build_release_package()
{
    log::run "Building release packages..."
    bazel build --config=gt_gen_release //Packaging:debian-gtgen-simulator \
        --local_cpu_resources=16 --disk_cache="${CACHEDIR}" \
        --experimental_disk_cache_gc_max_size=50G \
        --noshow_progress
    bazel build --config=gt_gen_release //Packaging:debian-gtgen-cli \
        --local_cpu_resources=16 --disk_cache="${CACHEDIR}" \
        --experimental_disk_cache_gc_max_size=50G \
        --noshow_progress
}

function install_debian_package()
{
    log::run "Cleaning previous installations..."
    rm -rf "${SIMULATOR_INSTALL_DIR}"
    mkdir -p "${SIMULATOR_INSTALL_DIR}"

    log::run "Installing GT-Gen packages..."
    dpkg-deb -xv bazel-bin/Packaging/gtgen-simulator_*.deb "${SIMULATOR_INSTALL_DIR}" >/dev/null 2>&1
    dpkg-deb -xv bazel-bin/Packaging/gtgen-cli_*.deb "${SIMULATOR_INSTALL_DIR}" >/dev/null 2>&1
}

function test_alks_controller_is_installed()
{
    log::run "Checking if ALKS Controller plugin is installed..."

    local file_path="${SIMULATOR_HOME}/GTGEN_DATA/Plugins/ALKSController.so"

    if [ -f "${file_path}" ]; then
        log::success "File ${file_path} is installed."
    else
        log::failure "ALKS Controller plugin is missing. Installation failed."
        exit 1
    fi
}

function test_gtgen_cli_is_installed()
{
    log::run "Checking if GT-Gen CLI is installed..."

    cli_output=$(gtgen_cli --version 2>/dev/null)
    sim_version=$(echo "$cli_output" | grep -oP 'GtGen-Simulator:\s*\K[0-9]+\.[0-9]+\.[0-9]+')

    # Read the version components from version.bzl, excluding debug versions
    major_version=$(grep '^GTGEN_SIMULATOR_VERSION_MAJOR =' Simulator/LibSimCore/version.bzl | awk -F'"' '{print $2}')
    minor_version=$(grep '^GTGEN_SIMULATOR_VERSION_MINOR =' Simulator/LibSimCore/version.bzl | awk -F'"' '{print $2}')
    patch_version=$(grep '^GTGEN_SIMULATOR_VERSION_PATCH =' Simulator/LibSimCore/version.bzl | awk -F'"' '{print $2}')
    expected_version="${major_version}.${minor_version}.${patch_version}"

    if [ "$sim_version" == "$expected_version" ]; then
        log::success "GtGen-Simulator version ($sim_version) matches the expected version ($expected_version)."
    else
        log::failure "Version mismatch: GtGen-Simulator version ($sim_version), expected version ($expected_version)"
        exit 1
    fi
}

function verify_simulation_reached_end()
{
    local csv_file="$1"
    local expected_end="10010" # Expected end time in milliseconds

    log::run "Verifying simulation reached the expected end (${expected_end} ms)..."

    # Get the first element of the last row
    last_value=$(tail -n 1 "$csv_file" | awk -F',' '{print $1}' | tr -d ' ')

    # Check if it matches the expected end time
    if [ "$last_value" == "$expected_end" ]; then
        log::success "Simulation reached the expected end time (${expected_end} ms)."
    else
        log::failure "ERROR: Simulation did not reach the expected end time: Last timestep found: ${last_value}, Expected: ${expected_end}"
        exit 1
    fi
}

function test_gtgen_with_basic_scenario()
{
    log::run "Running basic simulation..."
    local gtgen_data="${SIMULATOR_HOME}/GTGEN_DATA"
    local scenario="${SIMULATOR_HOME}/GTGEN_DATA/Scenarios/XOSC/example_simple_cut_in.xosc"
    local user_settings="${SIMULATOR_HOME}/GTGEN_DATA/UserSettings/UserSettings.ini"

    gtgen_cli -d "${gtgen_data}" -s "${scenario}" -u "${user_settings}" >/dev/null 2>&1
    local exit_code=$?

    if [ $exit_code -eq 0 ]; then
        log::success "gtgen_cli ran scenario ($scenario) successfully without errors."
    elif [ $exit_code -eq 139 ]; then
        log::failure "Segmentation fault detected (exit code 139)."
        exit 1
    else
        log::failure "gtgen_cli failed with exit code $exit_code."
        exit 1
    fi

    verify_simulation_reached_end "/tmp/gt-gen-simulator/Results/Cyclics_Run_0.csv"

    log::success "Release package creation and testing completed successfully."
}

function upload_artifacts()
{
    local git_tag
    git_tag=$(git tag --points-at HEAD)

    if [ -n "$git_tag" ]; then
        log::run "Copying release artifacts (Tag: $git_tag) to artifacts folder..."
        cp bazel-bin/Packaging/gtgen-simulator_*.deb "${BASEDIR}/artifacts"
        cp bazel-bin/Packaging/gtgen-cli_*.deb "${BASEDIR}/artifacts"

        echo "Release artifacts are available at:"
        ls "${BASEDIR}"/artifacts/gtgen-*.deb
    else
        echo "Skipping artifact upload for branch $BRANCH_NAME. Only tagged commits generate release packages."
    fi
}

function main()
{
    echo "Navigating to repository root..."
    cd "${ROOT_DIR}/../../.." || {
        log::failure "Failed to navigate to root directory"
        exit 1
    }

    cleanup_artifacts
    build_release_package
    install_debian_package
    test_alks_controller_is_installed
    test_gtgen_cli_is_installed
    test_gtgen_with_basic_scenario

    upload_artifacts
}

main "$@"
