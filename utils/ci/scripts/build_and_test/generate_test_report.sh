# *******************************************************************************
# Copyright (C) 2024, ANSYS, Inc.
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u

workspace="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." >/dev/null 2>&1 && pwd)"
log_dir=${workspace}
bazel_result=0

output_dir="${workspace}/test-report"
gtest_xml_log_dir=${output_dir}/gtest-output
bazel_log_file="${output_dir}/bazel_output.log"
bazel_test_results="${output_dir}/test_results.log"
bazel_test_results_page="test_results.html"

bazel_targets="//Simulator/... //Cli/..."
bazel_config="gt_gen"
bazel_excluded_targets=""
# Example bazel options: "--local_cpu_resources=16 --disk_cache=CACHEDIR --noshow_progress --nocache_test_results --noshow_progress --ui_event_filters=-info,-stderr"
bazel_options=""

bazel_logging="2>> ${bazel_log_file} 1>> ${bazel_test_results}"

overall_test_result_xml=${bazel_test_results_page%.html}.summary

function main()
{
    parse_arguments "$@"

    echo "Clean-up test result"

    cleanup_gtest_xml

    echo "Generate test html summary"

    bazel_test

    echo "Generate test html summary"
    generate_test_html_summary
}

function parse_arguments()
{
    while getopts "t:c:e:p:o:h" opt; do
        case ${opt} in
            h)
                info "Usage: "
                info "  -t      BAZEL_TARGETS              Bazel target"
                info "  -c      BAZEL_CONFIG               Bazel config"
                info "  -e      BAZEL_EXCLUDED_TARGETS     Bazel exclude targets"
                info "  -p      BAZEL_OPTIONS              Bazel options"
                info "  -o      OUTPUT_DIRECTORY           Output directory of the test logs"
                exit 0
                ;;
            t)
                bazel_targets="${OPTARG}"
                ;;
            c)
                bazel_config="${OPTARG}"
                ;;
            e)
                bazel_excluded_targets="${OPTARG}"
                ;;
            p)
                bazel_options="${OPTARG}"
                ;;
            o)
                output_dir="${workspace}/${OPTARG}"
                gtest_xml_log_dir=${output_dir}/gtest-output
                bazel_log_file="${output_dir}/bazel_output.log"
                bazel_test_results="${output_dir}/test_results.log"
                ;;
            \?)
                info "Invalid option: -${OPTARG}"
                exit 1
                ;;
            :)
                info "Invalid option: -${OPTARG} requires an argument!"
                exit 1
                ;;
        esac
    done
    shift $((OPTIND - 1))
}

function bazel_test()
{
    cd "${workspace}"

    echo ""
    echo "Writing test results to ${log_dir}/${bazel_test_results}"
    echo "Test results for --config=${bazel_config}" >>"${log_dir}"/"${bazel_test_results}"

    echo "Writing test logs (non cached only) to ${gtest_xml_log_dir}"

    bazel_cmd="bazel test --config=${bazel_config} ${bazel_options}  ${bazel_targets}  ${bazel_logging}"
    echo ""
    echo "Running bazel command: ${bazel_cmd}"
    echo ""
    eval "${bazel_cmd}"
    bazel_result=$(("$bazel_result" + $?))
}

function cleanup_gtest_xml()
{
    mkdir -pv "${gtest_xml_log_dir}"
    rm -vf "${gtest_xml_log_dir}"/*.xml
    rm -vf "${gtest_xml_log_dir}"/*.log
    rm -vf "${gtest_xml_log_dir}"/${overall_test_result_xml}
}

function get_bazel_testlogs_dir
{
    bazel_test_logs_root=$(bazel info --config=${bazel_config} bazel-testlogs)

    # Initialize an empty array to store the directory names.
    declare -a bazel_testlogs_dirs

    # Split the targets into an array based on spaces.
    IFS=' ' read -r -a bazel_targets_array <<<"$bazel_targets"

    for bazel_target in "${bazel_targets_array[@]}"; do
        # Extract the directory name between the first "//" and "/".
        dir_name=$(echo "$bazel_target" | sed -E 's#^//([^/]+)/.*#\1#')
        bazel_testlogs_dirs+=("${bazel_test_logs_root}/${dir_name}")
    done

    echo "${bazel_testlogs_dirs}"
}

function gather_gtest_results()
{

    bazel_test_logs_root=$(bazel info --config=${bazel_config} bazel-testlogs)

    local bazel_testlogs_dirs=$(get_bazel_testlogs_dir)

    echo "Testlogsdir: ${bazel_testlogs_dirs[@]}"

    for bazel_testlogs_dir in "${bazel_testlogs_dirs[@]}"; do
        if [ -d "${bazel_testlogs_dir}" ]; then
            cd "${bazel_testlogs_dir}"
            test_results=$(find . -name "test.xml")

            for xml_result in ${test_results}; do
                local parent_dir_name=$(dirname "${xml_result}")
                parent_dir_name=${parent_dir_name:2}     # remove first two character
                parent_dir_name=${parent_dir_name//\//_} # replace all / with _ to garantee uniqueness

                local source_gtest_xml=${xml_result}
                local source_gtest_log="${xml_result::-4}.log"
                local target_gtest_xml="${gtest_xml_log_dir}/${parent_dir_name}.xml"
                local target_gtest_html="${gtest_xml_log_dir}/${parent_dir_name}.html"

                echo "Copy gtest results of ${parent_dir_name}"
                cp "${source_gtest_xml}" "${target_gtest_xml}"
                # open up logfile and create an html side out of it
                echo "${source_gtest_log}" | aha >"${target_gtest_html}"

                local search_string="<testsuites"
                local replace_string="<testsuites\ testlog=\"gtest-output\/$(basename "${target_gtest_html}")\""

                sed -i "s/${search_string}/${replace_string}/g" "${target_gtest_xml}"
            done
        else
            echo "Could not find bazel-testlogs. Looks like bazel test was a failure."
        fi
    done
}

function generate_test_html_summary()
{
    gather_gtest_results

    # Html page generation
    if [ -d "${gtest_xml_log_dir}" ]; then
        echo "Generating test results html page..."
        echo '<?xml version="1.0" encoding="UTF-8"?>' >"${gtest_xml_log_dir}"/${overall_test_result_xml}
        echo '<main>' >>"${gtest_xml_log_dir}"/${overall_test_result_xml}
        for f in "${gtest_xml_log_dir}"/*.xml; do
            sed 1d "${f}" >>"${gtest_xml_log_dir}"/${overall_test_result_xml}
        done
        echo '</main>' >>"${gtest_xml_log_dir}"/${overall_test_result_xml}
        xsltproc "${workspace}"/utils/ci/scripts/utility/gtest2html.xslt "${gtest_xml_log_dir}"/${overall_test_result_xml} >"${log_dir}"/"${bazel_test_results_page}"
        bazel_result=$((bazel_result + $?))
    else
        # Tests were probably cached
        echo "No xml logs have been generated. Skipping html page generation..."
        echo "This is not the log you are looking for." >"${log_dir}"/"${bazel_test_results_page}"
        echo "" >>"${log_dir}"/"${bazel_test_results_page}"
        echo "No GTest xml log has been generated. Nothing to display." >>"${log_dir}"/"${bazel_test_results_page}"
    fi
    echo ""
}

main "$@"

exit 0
