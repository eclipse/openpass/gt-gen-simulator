# *******************************************************************************
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u
source "$(dirname "${BASH_SOURCE[0]}")/../helper_functions/bash_helper.sh"

genhtml_location=$(which genhtml)

bazel_config="gt_gen"
bazel_targets="//Simulator/... //Cli/..."
bazel_excluded_targets=""
output_dir="bazel-coverage"
# example bazel options: "--local_cpu_resources=16 --disk_cache=CACHEDIR --noshow_progress --nocache_test_results --noshow_progress --ui_event_filters=-info,-stderr"
bazel_options="--local_test_jobs=1"
report_title="Code Coverage Report for Gt-Gen-Simulator"

function main()
{
    parse_arguments "$@"

    pre_run

    code_coverage
}

function parse_arguments()
{
    while getopts "t:c:e:p:o:l:h" opt; do
        case ${opt} in
            h)
                info "Usage: "
                info "  -t      BAZEL_TARGETS              Bazel target"
                info "  -c      BAZEL_CONFIG               Bazel config"
                info "  -e      BAZEL_EXCLUDED_TARGETS     Bazel exclude targets"
                info "  -p      BAZEL_OPTIONS              Bazel options"
                info "  -o      OUTPUT_DIRECTORY           Output directory of the code coverage report"
                info "  -l      REPORT_TITLE               Display TITLE in the report header"
                exit 0
                ;;
            t)
                bazel_targets="${OPTARG}"
                ;;
            c)
                bazel_config="${OPTARG}"
                ;;
            e)
                bazel_excluded_targets="${OPTARG}"
                ;;
            p)
                bazel_options="${OPTARG}"
                ;;
            o)
                output_dir="${OPTARG}"
                ;;
            l)
                report_title="${OPTARG}"
                ;;
            \?)
                info "Invalid option: -${OPTARG}"
                exit 1
                ;;
            :)
                info "Invalid option: -${OPTARG} requires an argument!"
                exit 1
                ;;
        esac
    done
    shift $((OPTIND - 1))
}

function pre_run()
{
    if [[ -z ${genhtml_location} ]]; then
        log::warn "genhtml was not found on your system, can not check code-covrage"
        exit 1
    fi
}

function code_coverage()
{

    log::run "Running bazel code coverage ..."

    # note, to save CI time, we are not using--nocache_test_results
    bazel coverage --config=${bazel_config} --combined_report=lcov ${bazel_options} \
        --instrumentation_filter="-${bazel_excluded_targets}" ${bazel_targets}

    mkdir $output_dir

    lcov --remove bazel-out/_coverage/_coverage_report.dat 'external/*' '*/Tests/*' \
        -o $output_dir/filtered_coverage_report.dat >/dev/null 2>&1

    genhtml --show-details --legend --title "${report_title}" \
        --output-directory $output_dir $output_dir/filtered_coverage_report.dat

    ls -l $output_dir/index.html
    echo "Code coverage html report is saved at: $output_dir/index.html"
}

main "${@}"
