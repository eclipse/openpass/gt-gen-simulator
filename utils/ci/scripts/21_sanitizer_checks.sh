# *******************************************************************************
# Copyright (C) 2024-2025, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -e

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "/home/jenkins/cache/gtgen_simulator")

# This override the cache folder of bazel and bazelisk
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

echo "Run Bazel Sanitizer Checks ..."

bazel build --config=gt_gen_san //Cli/... //Simulator/... //Plugins/... \
    --local_cpu_resources=8 --disk_cache="${CACHEDIR}" \
    --experimental_disk_cache_gc_max_size=50G \
    --noshow_progress

bazel test --config=gt_gen_san //Cli/... //Simulator/... //Plugins/... \
    --local_cpu_resources=8 --disk_cache="${CACHEDIR}" \
    --experimental_disk_cache_gc_max_size=50G \
    --noshow_progress \
    -- -//Simulator/Tests/Integration/Simulator:tpm_plugin_integration_test
