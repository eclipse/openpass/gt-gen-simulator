#!/bin/bash

# *******************************************************************************
# Copyright (C) 2024-2025, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

set -ex

echo "Prepare ..."

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "/home/jenkins/cache/gtgen_simulator")

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Wipe old artifacts if exist
rm -rf "${BASEDIR}/artifacts"

# Debug prints
echo "Path of MYDIR: ${MYDIR}"
echo "Path of BASEDIR: ${BASEDIR}"
echo "Path of CACHEDIR: ${CACHEDIR}"

echo "Environment variables:"
env

git --version
bazel --version
clang-tidy --version
buildifier --version

whereis git
whereis bazel
whereis clang-tidy
whereis buildifier

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

# Update git submodules
echo "Checkout git submodules recursively ..."
git submodule update --init --recursive

echo "Prepare completed."
