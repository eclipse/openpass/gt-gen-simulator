<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
	<html>
		<head>
			<title>Unit Test Results</title>
			<style type="text/css">
				td#passed {
				color: white;
				font-weight: bold;
				background-color: #3D9970;
				}
				td#failed {
				color: white;
				font-weight: bold;
				background-color: #FF4136;
				}
				td#disabled {
				color: white;
				font-weight: bold;
				background-color: #FA7D07;
				}
				<!-- borrowod from here http://red-team-design.com/practical-css3-tables-with-rounded-corners/ -->
				body {
				width: 95%;
				margin: 40px auto;
				font-family: 'trebuchet MS', 'Lucida sans', Arial;
				font-size: 14px;
				color: #444;
				}
				table {
				*border-collapse: collapse; /* IE7 and lower */
				border-spacing: 0;
				width: 100%;
				}
				.bordered {
				border: solid #ccc 1px;
				-moz-border-radius: 6px;
				-webkit-border-radius: 6px;
				border-radius: 6px;
				-webkit-box-shadow: 0 1px 1px #ccc;
				-moz-box-shadow: 0 1px 1px #ccc;
				box-shadow: 0 1px 1px #ccc;
				}
				.bordered tr:hover {
				background: #fbf8e9;
				-o-transition: all 0.1s ease-in-out;
				-webkit-transition: all 0.1s ease-in-out;
				-moz-transition: all 0.1s ease-in-out;
				-ms-transition: all 0.1s ease-in-out;
				transition: all 0.1s ease-in-out;
				}
				.bordered td, .bordered th {
				border-left: 1px solid #ccc;
				border-top: 1px solid #ccc;
				padding: 10px;
				text-align: left;
				}
				.bordered th {
				background-color: #ebf4f0;
				-webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
				-moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset;
				box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
				border-top: none;
				text-shadow: 0 1px 0 rgba(255,255,255,.5);
				}
				.bordered td:first-child, .bordered th:first-child {
				border-left: none;
				}
				.bordered th:first-child {
				-moz-border-radius: 6px 0 0 0;
				-webkit-border-radius: 6px 0 0 0;
				border-radius: 6px 0 0 0;
				}
				.bordered th:last-child {
				-moz-border-radius: 0 6px 0 0;
				-webkit-border-radius: 0 6px 0 0;
				border-radius: 0 6px 0 0;
				}
				.bordered th:only-child{
				-moz-border-radius: 6px 6px 0 0;
				-webkit-border-radius: 6px 6px 0 0;
				border-radius: 6px 6px 0 0;
				}
				.bordered tr:last-child td:first-child {
				-moz-border-radius: 0 0 0 6px;
				-webkit-border-radius: 0 0 0 6px;
				border-radius: 0 0 0 6px;
				}
				.bordered tr:last-child td:last-child {
				-moz-border-radius: 0 0 6px 0;
				-webkit-border-radius: 0 0 6px 0;
				border-radius: 0 0 6px 0;
				}
			</style>
		</head>
		<body>
			<xsl:apply-templates/>
		</body>
	</html>
</xsl:template>

<xsl:template match="main">
	<xsl:variable name="total_tests" select="sum(testsuites/@tests)"/>
	<xsl:variable name="total_failures" select="sum(testsuites/@failures)"/>
	<xsl:variable name="total_disabled" select="sum(testsuites/@disabled)"/>
	<xsl:variable name="total_errors" select="sum(testsuites/@errors)"/>
	<xsl:variable name="total_success" select="$total_tests - $total_failures - $total_errors - $total_disabled"/>
	<xsl:variable name="total_time" select="sum(testsuites/@time)"/>
	<xsl:variable name="id_total_failures">
		<xsl:choose>
			<xsl:when test="$total_failures = 0">passed</xsl:when>
			<xsl:otherwise>failed</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="id_total_disabled">
		<xsl:choose>
			<xsl:when test="$total_disabled = 0">passed</xsl:when>
			<xsl:otherwise>disabled</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="id_total_errors">
		<xsl:choose>
			<xsl:when test="$total_errors = 0">passed</xsl:when>
			<xsl:otherwise>failed</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="id_total_success">
		<xsl:choose>
			<xsl:when test="$total_success > 0">passed</xsl:when>
			<xsl:otherwise>failed</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>



	<h1>Summary</h1>
	<table class="bordered" style="width:auto;">
		<tr>
			<td><b>Tests</b></td>
			<td style="text-align:right;"><xsl:value-of select="$total_tests"/></td>
		</tr>
		<tr>
			<td><b>Failures</b></td>
			<td id="{$id_total_failures}" style="text-align:right;"><xsl:value-of select="$total_failures"/></td>
		</tr>
		<tr>
			<td><b>Disabled</b></td>
			<td id="{$id_total_disabled}" style="text-align:right;"><xsl:value-of select="$total_disabled"/></td>
		</tr>
		<tr>
			<td><b>Errors</b></td>
			<td id="{$id_total_errors}" style="text-align:right;"><xsl:value-of select="$total_errors"/></td>
		</tr>
		<tr>
			<td><b>Success</b></td>
			<td id="{$id_total_success}" style="text-align:right;"><xsl:value-of select="$total_success"/></td>
		</tr>
		<tr>
			<td><b>Time</b></td>
			<td style="text-align:right;"><xsl:value-of select='format-number($total_time, "#.###")'/>s</td>
		</tr>
	</table>
	<xsl:if test="$total_failures > 0">
		<h1>Failures: <xsl:value-of select="$total_failures"/></h1>
		<xsl:for-each select="testsuites">
			<xsl:for-each select="testsuite">
				<xsl:variable name="failures" select="@failures"/>
				<xsl:if test="$failures != 0">
					<h3><xsl:value-of select="@name"/>
						<span style="font-size:12px;font-weight:normal;margin-left:1em">
							Tests <xsl:value-of select="@tests"/> |
							Failures <xsl:value-of select="@failures"/> |
							Disabled <xsl:value-of select="@disabled"/> |
							Errors <xsl:value-of select="@errors"/> |
							Time <xsl:value-of select="@time"/>s |
							Logfile:
							<a><xsl:attribute name="href">
									<xsl:value-of select="../@testlog"/>
								</xsl:attribute>
								<xsl:value-of select="../@testlog"/>
							</a>
						</span>
					</h3>
					<table class="bordered">
						<tr><th style="width:30%">Test</th>
							<th>Message</th>
							<th style="width:40px">Result</th>
							<th style="width:40px">Time</th>
						</tr>
						<xsl:for-each select="testcase">
							<xsl:if test="*">
								<tr>
									<td><xsl:value-of select="@name"/></td>
									<td id="failed">
										<xsl:for-each select="@* [name()!='classname' and name()!='name' and name()!='status' and name()!='time']">
											<xsl:value-of select="name()"/>=<xsl:value-of select="."/><br></br>
										</xsl:for-each>
										<xsl:for-each select="failure">
											<xsl:if test="@message">
												failure="<xsl:value-of select="@message"/>"
											</xsl:if>
										</xsl:for-each>
									</td>
									<td id="failed">FAILED</td>
									<td><xsl:value-of select="@time"/>s</td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</table>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:if>
	<br/><h1>Overall Summary</h1>
	<xsl:for-each select="testsuites">
		<xsl:for-each select="testsuite">
			<h3><xsl:value-of select="@name"/>
			<span style="font-size:12px;font-weight:normal;margin-left:1em">
				Tests <xsl:value-of select="@tests"/> |
				Failures <xsl:value-of select="@failures"/> |
				Disabled <xsl:value-of select="@disabled"/> |
				Errors <xsl:value-of select="@errors"/> |
				Time <xsl:value-of select="@time"/>s |
				Logfile:
				 <a><xsl:attribute name="href">
						<xsl:value-of select="../@testlog"/>
					</xsl:attribute>
					<xsl:value-of select="../@testlog"/>
                 </a>
			</span>
			</h3>
			<table class="bordered">
				<tr><th style="width:30%">Test</th>
					<th>Message</th>
					<th style="width:40px">Result</th>
					<th style="width:40px">Time</th>
				</tr>
				<xsl:for-each select="testcase">
					<tr>
						<td><xsl:value-of select="@name"/></td>
						<xsl:choose>
						<xsl:when test="*">
							<td id="failed">
								<xsl:for-each select="@* [name()!='classname' and name()!='name' and name()!='status' and name()!='time']">
									<xsl:value-of select="name()"/>=<xsl:value-of select="."/><br></br>
								</xsl:for-each>
								<xsl:for-each select="failure">
									<xsl:if test="@message">
										failure="<xsl:value-of select="@message"/>"
									</xsl:if>
								</xsl:for-each>
							</td>
							<td id="failed">FAILED</td>
						</xsl:when>
						<xsl:when test="starts-with(@name, 'DISABLED')">
							<td id="disabled">
								Test Disabled
							</td>
							<td id="disabled">
								N/A
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:for-each select="@* [name()!='classname' and name()!='name' and name()!='status' and name()!='time']">
									<xsl:value-of select="name()"/>=<xsl:value-of select="."/><br></br>
								</xsl:for-each>
								<xsl:for-each select="*">
									<xsl:value-of select="@message"/>
								</xsl:for-each>
							</td>
							<td id="passed">OK</td>
						</xsl:otherwise>
						</xsl:choose>
						<td><xsl:value-of select="@time"/>s</td>
						</tr>
					</xsl:for-each>
				</table>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
