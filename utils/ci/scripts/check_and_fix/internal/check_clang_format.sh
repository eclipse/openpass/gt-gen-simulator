# *******************************************************************************
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/usr/bin/env bash

set -u

source "$(dirname "${BASH_SOURCE[0]}")/../../helper_functions/bash_helper.sh"

perform_dry_run="${1-0}"
failed=0

targets=(
    "./Cli"
    "./Simulator"
)

function main()
{
    title="Checking clang format..."

    log::run "${title}"

    check_clang_format

    log::summary "${title}" "${failed}" "${perform_dry_run}"
    return "${failed}"
}

function check_clang_format()
{
    local valid_extensions_regex_escaped='.*\(\.c\|\.C\|\.cc\|\.cpp\|\.cxx\|\.h\|\.H\|\.hh\|\.hpp\|\.hxx\|\.cl\)$'

    format_tool=clang-format
    if which clang-format-11 >/dev/null; then
        format_tool=clang-format-11
    else
        log::warn "Error: clang-format-11 is not found"
        exit 1
    fi

    result=$({
        find "${targets[@]}" -regex "${valid_extensions_regex_escaped}" \
            | xargs -n1 -P$(nproc --all) "${format_tool}" --dry-run -style=file -fallback-style=none
    } 2>&1)

    readarray -t arr <<<"${result}"
    for ((i = 0; i < ${#arr[@]}; i += 3)); do
        if [[ -n ${arr[i]} ]]; then
            ((failed = failed + 1))
            if [[ "${perform_dry_run}" -eq 1 ]]; then
                log::sub_failure "Found clang format issue: ${arr[i]}"
            else
                log::sub_failure "Found clang format issue: ${arr[i]}"
            fi
        fi
    done

    if [[ "${perform_dry_run}" -eq 0 ]] && [[ -n "${result}" ]]; then
        result=$(find "${targets[@]}" -regex "${valid_extensions_regex_escaped}" \
            | xargs -n1 -P$(nproc --all) "${format_tool}" --i -style=file -fallback-style=none)
    fi
}

main "${@}"
