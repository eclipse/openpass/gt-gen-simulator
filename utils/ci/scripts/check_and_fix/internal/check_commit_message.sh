#!/bin/bash

# *******************************************************************************
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************
set -o errexit  # abort script when a command has a non-zero exit status
set -o nounset  # abort script when accessing an unbound variable
set -o pipefail # non-zero exit statuses within a pipe are not hidden
set -o errtrace # allow trapping errors that happen in functions and subshells

# Define the regex pattern for Conventional Commits
declare -ar VALID_COMMIT_PREFIXES=(
    "build"
    "chore"
    "devops"
    "docs"
    "feat"
    "fix"
    "perf"
    "refactor"
    "revert"
    "Revert"
    "style"
    "test"
)

# Join array with "|" character:
PATTERN_PREFIX="($(
    IFS=\|
    echo "${VALID_COMMIT_PREFIXES[*]}"
))"
readonly PATTERN_PREFIX

readonly PATTERN_OPTIONAL_SCOPE="(\(.+\))?!?"
readonly PATTERN_SEPARATOR=": "
readonly PATTERN_MIN_MESSAGE_LENGTH="1"
readonly PATTERN_MAX_MESSAGE_LENGTH="50"
readonly PATTERN_COMMIT_MESSAGE=".{${PATTERN_MIN_MESSAGE_LENGTH},${PATTERN_MAX_MESSAGE_LENGTH}}"

readonly PATTERN="^${PATTERN_PREFIX}${PATTERN_OPTIONAL_SCOPE}${PATTERN_SEPARATOR}${PATTERN_COMMIT_MESSAGE}$"

# Conventional Commit Types:
#
# build: Changes that affect the build system or external dependencies
# Example: build: update bazel to version 4.0.0
#
# chore: Other changes that don't modify src or test files.
# Example: chore: add commitlint configuration
#
# devops: Changes to devops configuration files and scripts
# Example: devops: update CI/CD pipeline
#
# docs: Documentation only changes.
# Example: docs: add API documentation for new feature
#
# feat: A new feature.
# Example: feat: add xxx feature
#
# fix: A bug fix.
# Example: fix: correct logic in xxx
#
# perf: A code change that improves performance.
# Example: perf: improve performance of xxx
#
# refactor: A code change that neither fixes a bug nor adds a feature.
# Example: refactor: update file structure
#
# revert: Reverts a previous commit.
# Example: revert: revert commit abc123 t
#
# style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc).
# Example: style: correct indentation in HTML files
#
# test: Adding missing tests or correcting existing tests.
# Example: test: add unit tests for xxx

source "$(dirname "${BASH_SOURCE[0]}")/../../helper_functions/bash_helper.sh"

FAILED=0            # number of FAILED checks
PERFORM_DRY_RUN="1" # no fix is applied, only check

TARGET_BRANCH="origin/main"
# Get the source branch name from the environment variable
if [[ -v SYSTEM_PULLREQUEST_SOURCEBRANCH ]]; then
    SOURCE_BRANCH="${SYSTEM_PULLREQUEST_SOURCEBRANCH}"
    if [[ "${SOURCE_BRANCH}" == refs/heads/* ]]; then
        SOURCE_BRANCH=${SOURCE_BRANCH#refs/heads/}
    fi
else
    # Fallback to git branch if SOURCE_BRANCH is not set
    SOURCE_BRANCH=$(git rev-parse --abbrev-ref HEAD)
fi

function check_commit_message()
{
    local commits
    commits=$(git log "${TARGET_BRANCH}"..HEAD --pretty=format:"%H %s")

    # Loop through each commit and its message
    while IFS= read -r line; do
        local hash
        hash=$(echo "${line}" | cut -d' ' -f1)

        local commit_message
        commit_message=$(echo "${line}" | cut -d' ' -f2-)

        # Skip if hash or message is empty
        if [[ -z "${hash}" ]] || [[ -z "${commit_message}" ]]; then
            continue
        fi

        # Skip merge commit messages
        if [[ "${commit_message}" =~ ^Merge[[:space:]]+(pull[[:space:]]+request|branch|commit|remote-tracking) ]]; then
            continue
        fi

        if [[ ! "${commit_message}" =~ ${PATTERN} ]]; then
            log::sub_failure "Invalid commit message: ${hash} - ${commit_message}"

            print_commit_error_reason "${commit_message}"

            ((FAILED += 1))
        fi
    done <<<"${commits}"
}

function print_commit_error_reason()
{
    local commit_message=$1
    # Attempt to check incorrect part of message in order from start to end of string
    if [[ ! "${commit_message}" =~ ^${PATTERN_PREFIX} ]]; then
        local matched_prefix=
        if [[ "${commit_message}" =~ ^([[:alnum:]]+) ]]; then
            matched_prefix="\`${BASH_REMATCH[1]}\` "
        fi
        log::sub_failure "Commit prefix ${matched_prefix}is wrong. Valid Conventional Commits prefixes include:"
        local prefix
        for prefix in "${VALID_COMMIT_PREFIXES[@]}"; do
            echo "           ${prefix}"
        done
    elif [[ ! "${commit_message}" =~ ^${PATTERN_PREFIX}${PATTERN_OPTIONAL_SCOPE}${PATTERN_SEPARATOR} ]]; then
        log::sub_failure "Either optional scope format incorrect or missing separator \`: \` between commit prefix and message."
    else
        log::sub_failure "Check commit message length. The first line should be between ${PATTERN_MIN_MESSAGE_LENGTH} and ${PATTERN_MAX_MESSAGE_LENGTH} characters after the prefix."
    fi
}

function main()
{
    local title="Checking commit messages on branch '$SOURCE_BRANCH' against Conventional Commits..."

    log::run "${title}"

    check_commit_message

    log::summary "${title}" "${FAILED}" "${PERFORM_DRY_RUN}"
    return "${FAILED}"
}

main "$@"
