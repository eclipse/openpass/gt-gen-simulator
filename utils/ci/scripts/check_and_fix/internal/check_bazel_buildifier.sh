# *******************************************************************************
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/usr/bin/env bash

set -u

source "$(dirname "${BASH_SOURCE[0]}")/../../helper_functions/bash_helper.sh"

perform_dry_run="${1-0}"
failed=0

targets=(
    "./Simulator"
    "./Cli"
)

buildifier_location=$(which buildifier)

function main()
{
    title="Checking Bazel buildifier..."

    log::run "${title}"

    apply_buildifier

    log::summary "${title}" "${failed}" "${perform_dry_run}"
    return "${failed}"
}

function check_buildifier_installation()
{
    if [[ -z ${buildifier_location} ]]; then
        log::warn "Can't find buildifier, exist formatting"
        exit 1
    fi
}

function apply_buildifier()
{
    declare -a BUILDIFIER_ARGS=(
        "--lint=warn"
        "--warnings=all"
        "--warnings=-native-cc"
        "--warnings=-module-docstring"
    )

    if [[ ${perform_dry_run} -eq 1 ]]; then
        BUILDIFIER_ARGS+=("--mode=check")
    else
        BUILDIFIER_ARGS+=("--mode=fix")
    fi

    local buildifier_result=$(buildifier "${BUILDIFIER_ARGS[@]}" -r "${targets[@]}" 2>&1)

    if [[ -n "${buildifier_result}" ]]; then
        printf '%s\n' "${buildifier_result[@]}"
        IFS=$'\n' # let's make sure we split on newline chars
        buildifier_result=(${buildifier_result})
        failed=${#buildifier_result[@]}
        unset IFS
    fi
}

main "${@}"
