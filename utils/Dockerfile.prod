# *******************************************************************************
# Copyright (C) 2025, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

# Dockerfile.prod for runtime environment allows to run gt-gen-simulator (CLI) in docker container

# Useage:
# 1. update the version and SHA 256 checksum of the simulator and cli in the Dockerfile
# 2. build the docker image using the following command and push it to the docker registry
# $ docker build -t avxautonomy/gt-gen-simulator:v9.0.1 -f utils/Dockerfile.prod .
# $ docker tag avxautonomy/gt-gen-simulator:v9.0.1 avxautonomy/gt-gen-simulator:latest
# $ docker push avxautonomy/gt-gen-simulator:v9.0.1
# $ docker push avxautonomy/gt-gen-simulator:latest

FROM ubuntu:20.04 AS runtime-base

SHELL ["/bin/sh", "-e", "-c"]

ARG SIMULATOR_VERSION=9.0.1
ARG SIMULATOR_SHA=d40850bdab95e92aea564ea11d64aee89c2f453a262329a6d6df6331d155e016

ARG CLI_VERSION=1.1.0
ARG CLI_SHA=90afa237c52ceb285147f4f3f46c7d00a49bdf8b7dbd967087c8db46a2cbea81

ARG ARTIFACTS_PATH=https://ci.eclipse.org/openpass/job/GT-Gen-Simulator-PreMerge-Gate/view/tags/job/v${SIMULATOR_VERSION}/lastSuccessfulBuild/artifact/artifacts
ARG CLI_FILE=gtgen-cli_${CLI_VERSION}_amd64.deb
ARG SIMULATOR_FILE=gtgen-simulator_${SIMULATOR_VERSION}_amd64.deb

RUN echo "Installing Dependencies..." &&  \
    apt-get --quiet update && \
    DEBIAN_FRONTEND=noninteractive apt-get --quiet install -y --no-install-recommends \
    wget \
    software-properties-common \
    zlib1g-dev \
    unzip \
    ca-certificates \
    && apt-get clean


RUN echo "Download and install the Debian package for Simulator..." &&   \
    wget -O /tmp/${SIMULATOR_FILE} ${ARTIFACTS_PATH}/${SIMULATOR_FILE} && \
    echo "${SIMULATOR_SHA} */tmp/${SIMULATOR_FILE}" | sha256sum --check - &&\
    dpkg -i /tmp/${SIMULATOR_FILE} && apt-get -f install -y

RUN echo "Download and install the Debian package for CLI..." &&  \
    wget -O /tmp/${CLI_FILE} ${ARTIFACTS_PATH}/${CLI_FILE} && \
    echo "${CLI_SHA} */tmp/${CLI_FILE}" | sha256sum --check - &&\
    dpkg -i /tmp/${CLI_FILE} && apt-get -f install -y


# Set environment variables
ENV SIMULATOR_HOME=/opt/gtgen_simulator
ENV CLI_HOME=/opt/gtgen_cli
ENV PATH="${SIMULATOR_HOME}:${CLI_HOME}:${PATH}"

# verify installation
RUN echo "Verify installation..." && \
    gtgen_cli --version

# Clean up
RUN rm -rf /tmp/*

ENTRYPOINT ["gtgen_cli"]
