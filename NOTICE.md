# Notices for Eclipse openpass

This content is produced and maintained by the Eclipse openpass project.

 * Project home: https://projects.eclipse.org/projects/automotive.openpass

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator

## Third-party Content
This project leverages the following third party content.


### bazel
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/bazel
- Version: 6.2.1

### bazel_rules
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/rules_pkg
- Version: patch version #TODO check version

### clara (Clara Command line parser)
- License: Boost Software License 1.0
- Repository: https://github.com/catchorg/Clara
- Version: 1.1.5

### googletest (Google Test)
- License: BSD 3-Clause "New" or "Revised" License
- Repository: https://github.com/google/googletest
- Version: 1.10.0

### GTGenCore
- License: Eclipse Public License 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/tags/v21.1.0

### OpenSCENARIO API (Parser)
- License: Apache-2.0
- Repository: https://github.com/RA-Consulting-GmbH/openscenario.api.test
- Commit: 5980e88
- Version: 1.4.0

### OpenScenarioEngine
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/tags/v13.0.0


### RoadLogicSuite
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/RoadLogicSuite
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite/-/releases/v4.1.4

### Yase
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/yase
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/yase/-/tags/v0.0.1

### nlohmann (JSON for Modern C++)
- License: MIT License
- Repository: https://github.com/nlohmann/json
- Version: 3.9.1

### stochastics library
- License: Eclipse Public License - v2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/stochastics-library
- Version: 0.11.1


## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
