/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/MapEngineOdr/map_engine_odr.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Profiling/profiling.h"
#include "Simulator/MapEngines/Utils/Map/OpenDrive/odr_to_gtgenmap_converter_creator.h"
#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::environment::api
{
using gtgen::core::environment::map::IAnyToGtGenMapConverter;
namespace map = gtgen::core::environment::map;

void MapEngineOdr::Load(const std::string& absolute_map_file_path,
                        const gtgen::core::service::user_settings::UserSettings& settings,
                        const mantle_api::MapDetails& map_details,
                        map::GtGenMap& gtgen_map,
                        gtgen::core::service::utility::UniqueIdProvider* unique_id_provider)
{
    auto converter_data = ConvertData(absolute_map_file_path, settings, map_details);

    std::unique_ptr<IAnyToGtGenMapConverter> converter{
        environment::map::open_drive::OdrToGtGenMapConverterCreator::Create(
            *unique_id_provider, converter_data, gtgen_map)};

    converter->Convert();
    Info("Map successfully loaded and converted.");
}

map::MapConverterData MapEngineOdr::FillConverterData(const gtgen::core::service::user_settings::UserSettings& settings,
                                                      const std::string& absolute_map_file_path,
                                                      const mantle_api::MapDetails& map_details)
{
    map::MapConverterData converter_data{};

    const auto& ground_truth = settings.ground_truth;
    converter_data.lane_marking_distance_in_m = ground_truth.lane_marking_distance_in_m;
    converter_data.lane_marking_downsampling = ground_truth.lane_marking_downsampling;
    converter_data.lane_marking_downsampling_epsilon = ground_truth.lane_marking_downsampling_epsilon;

    converter_data.absolute_map_path = absolute_map_file_path;

    if (const auto& json_map_details = dynamic_cast<const mantle_ext::JsonMapDetails*>(&map_details);
        json_map_details != nullptr)
    {
        converter_data.friction_patches = json_map_details->friction_patches;
    }

    return converter_data;
}
}  // namespace gtgen::simulator::environment::api
