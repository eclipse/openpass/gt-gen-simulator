/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_MAPENGINES_MAPENGINEODR_MAPENGINEODR_H
#define GTGEN_SIMULATOR_MAPENGINES_MAPENGINEODR_MAPENGINEODR_H

#include "Core/Environment/Map/Common/map_converter_data.h"
#include "Simulator/MapEngines/MapEngineBase/map_engine_base.h"

namespace gtgen::simulator::environment::api
{
namespace map = gtgen::core::environment::map;

class MapEngineOdr final : public MapEngineBase
{
  public:
    MapEngineOdr() = default;

    void Load(const std::string& absolute_map_file_path,
              const gtgen::core::service::user_settings::UserSettings& settings,
              const mantle_api::MapDetails& map_details,
              map::GtGenMap& gtgen_map,
              gtgen::core::service::utility::UniqueIdProvider* unique_id_provider) override;

  private:
    map::MapConverterData FillConverterData(const gtgen::core::service::user_settings::UserSettings& settings,
                                            const std::string& absolute_map_file_path,
                                            const mantle_api::MapDetails& map_details) override;
};

}  // namespace gtgen::simulator::environment::api

#endif  // GTGEN_SIMULATOR_MAPENGINES_MAPENGINEODR_MAPENGINEODR_H
