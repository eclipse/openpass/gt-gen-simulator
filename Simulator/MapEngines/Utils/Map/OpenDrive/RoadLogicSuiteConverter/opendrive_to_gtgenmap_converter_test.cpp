/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/opendrive_to_gtgenmap_converter_test_fixture.h"

namespace gtgen::simulator::environment::map::open_drive
{
using units::literals::operator""_m;
using units::literals::operator""_rad;
using gtgen::core::environment::map::GtGenMap;
using gtgen::core::environment::map::LaneGroup;

TEST_F(OpendriveToGtGenMapConverterTest, GivenValidOdrMap_WhenCreateGtGenMapFromFile_ThenMapTypeIsOpenDrive)
{
    const auto map_name = "simple_road.xodr";
    GetGtGenMap(map_name);
    EXPECT_TRUE(gtgen_map->IsOpenDrive());
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenMapsWithAndWithoutGeoReference_WhenParsingMap_ThenGeoReferenceIsCorrectlyParsed)
{
    std::vector<std::tuple<std::string, std::string>> test_cases{
        {"simple_road.xodr", ""},
        {"3kmStraight.xodr",
         "+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=-194000 +y_0=-5346000 +datum=WGS84 +units=m +no_defs"}};

    for (const auto& test_case : test_cases)
    {
        GetGtGenMap(std::get<0>(test_case));

        EXPECT_EQ(gtgen_map->projection_string, std::get<1>(test_case));
    }
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenValidMapWithLaneWithTwoRoadmarks_WhenCreateGtGenMapFromFile_ThenBothRoadmarksAreParsed)
{
    GetGtGenMap("simple_road.xodr");

    ASSERT_EQ(gtgen_map->GetLaneGroups().size(), 2);
    ASSERT_EQ(gtgen_map->GetLanes().size(), 4);
    ASSERT_EQ(gtgen_map->GetLaneBoundaries().size(), 6);

    const auto& lane_group = gtgen_map->GetLaneGroups().at(0);
    EXPECT_EQ(lane_group.lane_boundary_ids.size(), 3);

    const auto& lane = gtgen_map->GetLanes().at(0);
    ASSERT_EQ(lane.left_lane_boundaries.size(), 1);
    ASSERT_EQ(lane.right_lane_boundaries.size(), 1);

    const auto& first_right_lane_boundary_id = lane.right_lane_boundaries.at(0);
    const auto& second_right_lane_boundary_id = lane.right_lane_boundaries.at(0);
    const auto& first_right_lane_boundary = gtgen_map->GetLaneBoundary(first_right_lane_boundary_id);
    const auto& second_right_lane_boundary = gtgen_map->GetLaneBoundary(second_right_lane_boundary_id);
    EXPECT_EQ(first_right_lane_boundary.type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(second_right_lane_boundary.type, LaneBoundary::Type::kDashedLine);
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenValidOdrMap_WhenCreateGtGenMapFromFile_ThenCorrectLeftRightAdjacencyCountIsAsExpected)
{
    // map, left, right
    std::vector<std::tuple<std::string, std::size_t, std::size_t>> test_cases{{"simple_road.xodr", 4, 2},
                                                                              {"junction_example.xodr", 4, 4}};

    for (const auto& [map_name, expected_left_lanes_count, expected_right_lane_count] : test_cases)
    {
        GetGtGenMap(map_name);

        std::size_t left_adjacent_lanes = 0;
        std::size_t right_adjacent_lanes = 0;

        for (const auto& lane : gtgen_map->GetLanes())
        {
            left_adjacent_lanes += lane.left_adjacent_lanes.size();
            right_adjacent_lanes += lane.right_adjacent_lanes.size();
        }

        EXPECT_EQ(left_adjacent_lanes, expected_left_lanes_count) << map_name;
        EXPECT_EQ(right_adjacent_lanes, expected_right_lane_count) << map_name;
    }
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenValidOdrMapsAndNoDownsampling_WhenCreateGtGenMapFromFile_ThenCenterLineSampleCountIsAsExpected)
{
    const std::vector<std::pair<std::string, int>> test_cases{{"simple_road.xodr", 1001},
                                                              {"junction_example.xodr", 31}};

    double lane_marking_distance_in_m = 1;
    double lane_marking_downsampling_epsilon = 0.01;
    bool lane_marking_downsampling = false;

    for (const auto& [map_name, expected_centerline_points] : test_cases)
    {
        GetGtGenMap(map_name, lane_marking_distance_in_m, lane_marking_downsampling_epsilon, lane_marking_downsampling);
        const auto& gtgen_lanes = gtgen_map->GetLanes();
        ASSERT_GE(gtgen_lanes.size(), 1) << map_name;

        const auto& gtgen_lane = gtgen_lanes.front();
        const auto& centerline_sample_count = gtgen_lane.center_line.size();
        EXPECT_EQ(centerline_sample_count, expected_centerline_points) << map_name;
    }
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenValidOdrMapsWithDownsampling_WhenCreateGtGenMapFromFile_ThenCenterLineSampleCountIsAsExpected)
{
    const std::vector<std::pair<std::string, int>> test_cases{{"simple_road.xodr", 2}, {"junction_example.xodr", 5}};

    double lane_marking_distance_in_m = 0.01;
    double lane_marking_downsampling_epsilon = 1.0;
    bool lane_marking_downsampling = true;

    for (const auto& [map_name, expected_centerline_points] : test_cases)
    {
        GetGtGenMap(map_name, lane_marking_distance_in_m, lane_marking_downsampling_epsilon, lane_marking_downsampling);
        const auto& gtgen_lanes = gtgen_map->GetLanes();
        ASSERT_GE(gtgen_lanes.size(), 1) << map_name;

        const auto& gtgen_lane = gtgen_lanes.front();
        const auto& centerline_sample_count = gtgen_lane.center_line.size();
        EXPECT_EQ(centerline_sample_count, expected_centerline_points) << map_name;
    }
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenOdrMapWithJunctionAndLaneSplit_WhenCreateGtGenMapFromFile_ThenCenterLineHasEnoughSamples)
{
    const std::vector<std::string> map_names{{"junction_example.xodr"}};

    double lane_marking_distance_in_m = 0.4;
    double lane_marking_downsampling_epsilon = 1.0;
    bool lane_marking_downsampling = false;

    for (const auto& map_name : map_names)
    {
        GetGtGenMap(map_name, lane_marking_distance_in_m, lane_marking_downsampling_epsilon, lane_marking_downsampling);
        const auto& gtgen_lanes = gtgen_map->GetLanes();
        ASSERT_GT(gtgen_lanes.size(), 1);

        for (const auto& gtgen_lane : gtgen_lanes)
        {
            const auto& centerline_sample_count = gtgen_lane.center_line.size();
            EXPECT_GT(centerline_sample_count, 2);
        }
    }
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenValidOdrMap_WhenCreateGtGenMapFromFile_ThenCenterLineDirectionIsAsExpected)
{
    //.second is length of lanes in the map
    std::vector<std::string> test_cases{{"simple_road.xodr"}};

    for (const auto& test_case : test_cases)
    {
        GetGtGenMap(test_case);

        for (const auto& lane : gtgen_map->GetLanes())
        {
            for (std::uint64_t i = 1; i < lane.center_line.size(); ++i)
            {
                if (lane.center_line[0].y < 0_m)
                {
                    EXPECT_GT(lane.center_line[i].x, lane.center_line[i - 1].x);
                }
                else
                {
                    EXPECT_LT(lane.center_line[i].x, lane.center_line[i - 1].x);
                }
            }
        }
    }
}

TEST_F(OpendriveToGtGenMapConverterTest, GivenValidOdrMap_WhenCreateGtGenMapFromFile_ThenNumberOfBoundariesIsAsExpected)
{
    std::vector<std::pair<std::string, std::size_t>> test_cases{{"simple_road.xodr", 6}, {"junction_example.xodr", 10}};

    for (const auto& [map_name, expected_number_of_boundaries] : test_cases)
    {
        GetGtGenMap(map_name);

        std::size_t converted_boundaries = 0;
        for (const auto& lane_group : gtgen_map->GetLaneGroups())
        {
            converted_boundaries += lane_group.lane_boundary_ids.size();
        }

        EXPECT_EQ(converted_boundaries, expected_number_of_boundaries) << map_name;
    }
}

TEST_F(OpendriveToGtGenMapConverterTest,
       GivenValidOdrMapWithDoubleLaneBoundary_WhenCreateGtGenMapFromFile_ThenBoundaryIdsAreCorrect)
{
    const auto& map = GetGtGenMap("simple_road.xodr");

    ASSERT_EQ(map.GetLaneGroups().size(), 2);

    const auto& lane_group = map.GetLaneGroups()[0];
    ASSERT_EQ(lane_group.lane_boundary_ids.size(), 3);
}

}  // namespace gtgen::simulator::environment::map::open_drive
