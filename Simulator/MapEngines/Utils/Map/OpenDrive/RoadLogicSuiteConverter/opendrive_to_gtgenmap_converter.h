/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_OPENDRIVETOGTGENMAPCONVERTER_H
#define GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_OPENDRIVETOGTGENMAPCONVERTER_H

#include "Core/Environment/Map/Common/i_gtgenmap_converter_base.h"
#include "Core/Environment/Map/MapApiConverter/mapapi_to_gtgenmap_converter.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <RoadLogicSuite/road_logic_suite.h>

#include <memory>

namespace gtgen::simulator::environment::map::open_drive
{
using gtgen::core::environment::map::GtGenMap;
using gtgen::core::environment::map::IGtGenMapConverterBase;
using gtgen::core::environment::map::MapApiToGtGenMapConverter;

class OpendriveToGtGenMapConverter : public IGtGenMapConverterBase
{
  public:
    using IGtGenMapConverterBase::IGtGenMapConverterBase;

    OpendriveToGtGenMapConverter() = delete;
    OpendriveToGtGenMapConverter(const OpendriveToGtGenMapConverter&) = delete;
    OpendriveToGtGenMapConverter(OpendriveToGtGenMapConverter&&) = delete;
    OpendriveToGtGenMapConverter& operator=(const OpendriveToGtGenMapConverter&) = delete;
    OpendriveToGtGenMapConverter& operator=(OpendriveToGtGenMapConverter&&) = delete;
    ~OpendriveToGtGenMapConverter() override = default;

    /// @copydoc IAnyToGtGenMapConverter::GetNativeToGtGenTrafficLightIdMap()
    std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToGtGenTrafficLightIdMap() const override;

  private:
    void PreConvert() override;
    void ConvertInternal() override;
    void PostConvert() override;

    std::shared_ptr<road_logic_suite::RoadLogicSuite> road_logic_suite_;
};

}  // namespace gtgen::simulator::environment::map::open_drive

#endif  // GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_OPENDRIVETOGTGENMAPCONVERTER_H
