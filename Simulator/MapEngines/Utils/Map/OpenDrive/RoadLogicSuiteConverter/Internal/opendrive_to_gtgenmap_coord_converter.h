/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_INTERNAL_OPENDRIVETOGTGENMAPCOORDCONVERTER_H
#define GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_INTERNAL_OPENDRIVETOGTGENMAPCOORDCONVERTER_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Common/position.h>
#include <RoadLogicSuite/road_logic_suite.h>
#include <units.h>

#include <memory>
#include <optional>

namespace gtgen::simulator::environment::map::open_drive
{

using gtgen::core::environment::map::IConverter;
using UnderlyingMapCoordinate =
    std::variant<mantle_api::OpenDriveLanePosition, mantle_api::OpenDriveRoadPosition, mantle_api::LatLonPosition>;

class OpenDriveToGtGenMapCoordConverter
    : public IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>
{
  public:
    explicit OpenDriveToGtGenMapCoordConverter(
        const std::shared_ptr<road_logic_suite::RoadLogicSuite>& road_logic_suite);

    using IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>::Convert;

    std::optional<mantle_api::Vec3<units::length::meter_t>> Convert(
        const UnderlyingMapCoordinate& coordinate) const override;

    std::optional<UnderlyingMapCoordinate> Convert(
        const mantle_api::Vec3<units::length::meter_t>& coordinate) const override;

    mantle_api::Orientation3<units::angle::radian_t> GetLaneOrientation(
        const mantle_api::OpenDriveLanePosition& open_drive_lane_position) const override;

    mantle_api::Orientation3<units::angle::radian_t> GetRoadOrientation(
        const mantle_api::OpenDriveRoadPosition& open_drive_road_position) const override;

  private:
    std::shared_ptr<road_logic_suite::RoadLogicSuite> road_logic_suite_;
};

}  // namespace gtgen::simulator::environment::map::open_drive

#endif  // GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_INTERNAL_OPENDRIVETOGTGENMAPCOORDCONVERTER_H
