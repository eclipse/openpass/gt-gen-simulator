<?xml version="1.0" encoding="UTF-8"?>
<OpenSCENARIO>
  <FileHeader revMajor="1" revMinor="1" date="2024-04-10T12:00:00"
    description="This scenario loads one mounted sign" author="ANSYS"/>
  <CatalogLocations>
    <VehicleCatalog>
      <Directory path="./Catalogs/Vehicles"/>
    </VehicleCatalog>
    <MiscObjectCatalog>
      <Directory path="./Catalogs/MiscObjects"/>
    </MiscObjectCatalog>
  </CatalogLocations>
  <RoadNetwork>
    <LogicFile filepath="../../Maps/crossing_example.xodr"/>
  </RoadNetwork>
  <Entities>
    <ScenarioObject name="Ego">
      <CatalogReference catalogName="VehicleCatalog" entryName="Car"/>
    </ScenarioObject>
    <ScenarioObject name="Vehicle">
      <CatalogReference catalogName="VehicleCatalog" entryName="Car"/>
    </ScenarioObject>
    <ScenarioObject name="SpeedLimit60">
      <CatalogReference catalogName="TrafficSignCatalog" entryName="traffic_sign">
        <ParameterAssignments>
          <ParameterAssignment parameterRef="type" value="274"/>
          <ParameterAssignment parameterRef="sub_type" value="56"/>
        </ParameterAssignments>
      </CatalogReference>
    </ScenarioObject>
    <ScenarioObject name="TrucksOnly">
      <CatalogReference catalogName="TrafficSignCatalog" entryName="supplementary_sign">
        <ParameterAssignments>
          <ParameterAssignment parameterRef="mounted_to" value="SpeedLimit60"/>
          <ParameterAssignment parameterRef="type" value="1010"/>
          <ParameterAssignment parameterRef="sub_type" value="51"/>
        </ParameterAssignments>
      </CatalogReference>
    </ScenarioObject>
  </Entities>
  <Storyboard>
    <Init>
      <Actions>
        <Private entityRef="Ego">
          <PrivateAction>
            <TeleportAction>
              <Position>
                <LanePosition roadId="1" laneId="1" offset="0.0" s="0.0"/>
              </Position>
            </TeleportAction>
          </PrivateAction>
          <PrivateAction>
            <LongitudinalAction>
              <SpeedAction>
                <SpeedActionDynamics dynamicsShape="step" dynamicsDimension="time" value="0"/>
                <SpeedActionTarget>
                  <AbsoluteTargetSpeed value="0.0"/>
                </SpeedActionTarget>
              </SpeedAction>
            </LongitudinalAction>
          </PrivateAction>
        </Private>
        <Private entityRef="Vehicle">
          <PrivateAction>
            <TeleportAction>
              <Position>
                <LanePosition roadId="1" laneId="-1" offset="0.0" s="0.0">
                  <Orientation type="relative"/>
                </LanePosition>
              </Position>
            </TeleportAction>
          </PrivateAction>
          <PrivateAction>
            <LongitudinalAction>
              <SpeedAction>
                <SpeedActionDynamics dynamicsShape="step" dynamicsDimension="time" value="0"/>
                <SpeedActionTarget>
                  <AbsoluteTargetSpeed value="5.0"/>
                </SpeedActionTarget>
              </SpeedAction>
            </LongitudinalAction>
          </PrivateAction>
        </Private>
        <Private entityRef="SpeedLimit60">
          <PrivateAction>
            <TeleportAction>
              <Position>
                <LanePosition roadId="1" laneId="-1" offset="0.0" s="48.0">
                  <Orientation h="3.1415" type="relative"/>
                </LanePosition>
              </Position>
            </TeleportAction>
          </PrivateAction>
        </Private>
      </Actions>
    </Init>
    <Story name="DecelerateStory">
      <Act name="DeceletateAct">
        <ManeuverGroup maximumExecutionCount="1" name="DecelerateManeuverGroup">
          <Actors selectTriggeringEntities="false">
            <EntityRef entityRef="Vehicle"/>
          </Actors>
          <Maneuver name="DecelerateManeuver">
            <Event name="DecelerateEvent" priority="override">
              <Action name="DecelerateAction">
                <PrivateAction>
                  <LongitudinalAction>
                    <SpeedAction>
                      <SpeedActionDynamics dynamicsShape="linear" value="-1" dynamicsDimension="rate"/>
                      <SpeedActionTarget>
                        <AbsoluteTargetSpeed value="0"/>
                      </SpeedActionTarget>
                    </SpeedAction>
                  </LongitudinalAction>
                </PrivateAction>
              </Action>
              <StartTrigger>
                <ConditionGroup>
                  <Condition name="DecelerateStartCondition" delay="0" conditionEdge="rising">
                    <ByEntityCondition>
                      <TriggeringEntities triggeringEntitiesRule="any">
                        <EntityRef entityRef="SpeedLimit60"/>
                      </TriggeringEntities>
                      <EntityCondition>
                        <RelativeDistanceCondition entityRef="Vehicle" relativeDistanceType="longitudinal" value="12.0"
                          freespace="true" rule="lessThan" coordinateSystem="entity"/>
                      </EntityCondition>
                    </ByEntityCondition>
                  </Condition>
                </ConditionGroup>
              </StartTrigger>
            </Event>
          </Maneuver>
        </ManeuverGroup>
        <StartTrigger>
          <ConditionGroup>
            <Condition name="CutInActStart" delay="0" conditionEdge="none">
              <ByValueCondition>
                <SimulationTimeCondition value="0" rule="greaterOrEqual"/>
              </ByValueCondition>
            </Condition>
          </ConditionGroup>
        </StartTrigger>
      </Act>
    </Story>
    <StopTrigger>
      <ConditionGroup>
        <Condition name="End" delay="0" conditionEdge="rising">
          <ByValueCondition>
            <SimulationTimeCondition value="30" rule="greaterThan"/>
          </ByValueCondition>
        </Condition>
      </ConditionGroup>
    </StopTrigger>
  </Storyboard>
</OpenSCENARIO>
