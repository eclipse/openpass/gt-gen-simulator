<?xml version="1.0" encoding="utf-8"?>
<OpenScenario>
    <FileHeader revMajor="1" revMinor="1"
        date="2025-01-10T10:00:00"
        description="Ego vehicle cruises in the adjacent lane and performs a lane change maneuver to cut in front of the traffic vehicle."
        author="Ansys Inc.">
    </FileHeader>
    <ParameterDeclarations>
        <ParameterDeclaration name="egoSpeed" parameterType="double" value="40.0"/>
        <ParameterDeclaration name="trafficSpeed" parameterType="double" value="${$egoSpeed - 20.0}"/>
    </ParameterDeclarations>
    <CatalogLocations>
        <VehicleCatalog>
            <Directory path="./Catalogs/Vehicles"/>
        </VehicleCatalog>
    </CatalogLocations>
    <RoadNetwork>
        <LogicFile filepath="../../Maps/simple_road_straight.xodr"/>
    </RoadNetwork>
    <Entities>
        <ScenarioObject name="Ego">
            <CatalogReference catalogName="VehicleCatalog" entryName="Car"></CatalogReference>
        </ScenarioObject>
        <ScenarioObject name="TrafficVehicle">
            <CatalogReference catalogName="VehicleCatalog" entryName="Car"></CatalogReference>
        </ScenarioObject>
    </Entities>
    <Storyboard>
        <Init>
            <Actions>
                <Private entityRef="Ego">
                    <PrivateAction>
                        <TeleportAction>
                            <Position>
                                <LanePosition roadId="0" laneId="-2" offset="0.0" s="5.0"></LanePosition>
                            </Position>
                        </TeleportAction>
                    </PrivateAction>
                    <PrivateAction>
                        <LongitudinalAction>
                            <SpeedAction>
                                <SpeedActionDynamics dynamicsShape="linear" dynamicsDimension="time"
                                    value="10"/>
                                <SpeedActionTarget>
                                    <AbsoluteTargetSpeed value="$egoSpeed"/>
                                </SpeedActionTarget>
                            </SpeedAction>
                        </LongitudinalAction>
                    </PrivateAction>
                </Private>
                <Private entityRef="TrafficVehicle">
                    <PrivateAction>
                        <TeleportAction>
                            <Position>
                                <LanePosition roadId="0" laneId="-1" offset="0.0" s="100.0"></LanePosition>
                            </Position>
                        </TeleportAction>
                    </PrivateAction>
                    <PrivateAction>
                        <LongitudinalAction>
                            <SpeedAction>
                                <SpeedActionDynamics dynamicsShape="linear" dynamicsDimension="time"
                                    value="10"/>
                                <SpeedActionTarget>
                                    <AbsoluteTargetSpeed value="$trafficSpeed"/>
                                </SpeedActionTarget>
                            </SpeedAction>
                        </LongitudinalAction>
                    </PrivateAction>
                </Private>
            </Actions>
        </Init>
        <Story name="CutInStory">
            <Act name="CutInAct">
                <ManeuverGroup maximumExecutionCount="1" name="CutInManeuverGroup">
                    <Actors selectTriggeringEntities="true">
                        <EntityRef entityRef="Ego"/>
                    </Actors>
                    <Maneuver name="CutInManeuver">
                        <Event name="CutInEvent" priority="overwrite">
                            <Action name="CutInAction">
                                <PrivateAction>
                                    <LateralAction>
                                        <LaneChangeAction>
                                            <!-- Ego changes to next lane of Traffic-->
                                            <LaneChangeActionDynamics dynamicsShape="sinusoidal"
                                                value="3"
                                                dynamicsDimension="rate"/>
                                            <LaneChangeTarget>
                                                <RelativeTargetLane entityRef="TrafficVehicle"
                                                    value="0"/>
                                            </LaneChangeTarget>
                                        </LaneChangeAction>
                                    </LateralAction>
                                </PrivateAction>
                            </Action>
                            <StartTrigger>
                                <ConditionGroup>
                                    <Condition name="AfterEgoIsClose" delay="0" conditionEdge="none">
                                        <ByEntityCondition>
                                            <TriggeringEntities triggeringEntitiesRule="any">
                                                <EntityRef entityRef="Ego"/>
                                            </TriggeringEntities>
                                            <EntityCondition>
                                                <RelativeDistanceCondition value="1"
                                                    relativeDistanceType="longitudinal"
                                                    entityRef="TrafficVehicle"
                                                    freespace="false" coordinateSystem="entity"
                                                    rule="lessThan"
                                                    routingAlgorithm="shortest"/>
                                            </EntityCondition>
                                        </ByEntityCondition>
                                    </Condition>
                                </ConditionGroup>
                            </StartTrigger>
                        </Event>
                    </Maneuver>
                </ManeuverGroup>
            </Act>
        </Story>
        <!-- should  stop when ego cuts traffic -->
        <StopTrigger>
            <ConditionGroup>
                <Condition name="End" delay="0" conditionEdge="rising">
                    <ByValueCondition>
                        <SimulationTimeCondition value="25" rule="greaterThan"/>
                    </ByValueCondition>
                </Condition>
            </ConditionGroup>
        </StopTrigger>
    </Storyboard>
</OpenScenario>
