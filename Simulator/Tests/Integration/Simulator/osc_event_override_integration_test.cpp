/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithOverrideEvent_WhenInitAndStepSimulatorUntilActionIsFinished_ThenFirstEventIsOverridden)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_override_action.xosc"};

    constexpr double original_lane_y_position = -1.75;
    constexpr double lane_y_position_after_override = -3.3252592592592594;

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    auto last_ground_truth = GetLastGroundTruth();

    // Initial state
    ASSERT_EQ(last_ground_truth.moving_object_size(), 2);
    const auto& traffic_vehicle_init = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_init.base().position().y(), original_lane_y_position);

    // Start first lane change
    RunFor(std::chrono::milliseconds{5100});
    last_ground_truth = GetLastGroundTruth();
    const auto& traffic_vehicle_action1_start = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_action1_start.base().position().y(), original_lane_y_position);

    // Override with second lane change
    RunFor(std::chrono::milliseconds{1500});
    last_ground_truth = GetLastGroundTruth();
    const auto& traffic_vehicle_action1_end = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_action1_end.base().position().y(), lane_y_position_after_override);

    // Complete second lane change
    RunFor(std::chrono::milliseconds{3000});
    last_ground_truth = GetLastGroundTruth();
    const auto& traffic_vehicle_action2_end = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_action2_end.base().position().y(), original_lane_y_position);
}

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithSkipEvent_WhenInitAndStepSimulatorUntilActionIsFinished_ThenEventIsSkipped)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_skip_action.xosc"};

    constexpr double original_lane_y_position = -1.75;
    constexpr double middle_lane_y_position = -3.5;
    constexpr double final_lane_y_position = -5.25;

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    auto last_ground_truth = GetLastGroundTruth();

    // Initial state
    ASSERT_EQ(last_ground_truth.moving_object_size(), 2);
    const auto& traffic_vehicle_init = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_init.base().position().y(), original_lane_y_position);

    // Start lane change
    RunFor(std::chrono::milliseconds{5100});
    last_ground_truth = GetLastGroundTruth();
    const auto& traffic_vehicle_action1_start = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_action1_start.base().position().y(), original_lane_y_position);

    // Middle of lane change. Second lane change is skipped
    RunFor(std::chrono::milliseconds{1500});
    last_ground_truth = GetLastGroundTruth();
    const auto& traffic_vehicle_action1_middle = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_action1_middle.base().position().y(), middle_lane_y_position);

    // Complete lane change
    RunFor(std::chrono::milliseconds{3000});
    last_ground_truth = GetLastGroundTruth();
    const auto& traffic_vehicle_action1_end = last_ground_truth.moving_object(1);
    EXPECT_DOUBLE_EQ(traffic_vehicle_action1_end.base().position().y(), final_lane_y_position);
}

}  // namespace gtgen::simulator
