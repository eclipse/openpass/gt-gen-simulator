/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

#include <cmath>

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

static const double kSineYTolerance = 2e-2;

void sineCheck(double x, double y, long unsigned int end_idx, std::vector<std::pair<double, double>>& trajectory_xy)
{
    auto theta = M_PI * ((x - trajectory_xy[0].first) / (trajectory_xy[end_idx].first - trajectory_xy[0].first));
    auto amplitude = ((trajectory_xy[0].second - trajectory_xy[end_idx].second) * 0.5);
    auto offset = ((trajectory_xy[end_idx].second + trajectory_xy[0].second) * 0.5);
    auto expected = amplitude * cos(theta) + offset;
    EXPECT_NEAR(expected, y, kSineYTolerance);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithLaneChangeAction_WhenEgoIsCloseToTraffic_ThenItChangesLaneToLeftAsSineWaveInYDirectionWithDistanceParam)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "lane_change_action_left_sine_distance.xosc"};

    const double ego_start_y_position = -5.25;
    const double traffic_y_position = -1.75;

    InitSimulator("FastestRun.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement);
    SpinNTimes(1);

    // check ego's and traffic's original state
    ASSERT_EQ(GetLastGroundTruth().moving_object_size(), 2);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(0).base().position().y(), ego_start_y_position);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(1).base().position().y(), traffic_y_position);

    const double step_of_lane_change = 971;
    const double step_of_lane_change_complete = 1099;

    SpinNTimes(step_of_lane_change - 1);

    std::vector<std::pair<double, double>> trajectory_xy;

    // store the trajectory
    for (auto itr = step_of_lane_change; itr <= step_of_lane_change_complete; ++itr)
    {
        // capture the points in y direction as function of adjusted x
        auto x = GetLastGroundTruth().moving_object(0).base().position().x();
        auto y = GetLastGroundTruth().moving_object(0).base().position().y();
        trajectory_xy.emplace_back(x, y);
        SpinNTimes(1);
    }
    auto end_idx = trajectory_xy.size() - 1;

    // check trajectory nature
    for (const auto& each : trajectory_xy)
    {
        sineCheck(each.first, each.second, end_idx, trajectory_xy);
    }

    // check ego's state after lane change, it shall reach traffic position in y direction
    EXPECT_NEAR(trajectory_xy[end_idx].second, traffic_y_position, kSineYTolerance);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithLaneChangeAction_WhenEgoIsCloseToTraffic_ThenItChangesLaneToRightAsSineWaveInYDirectionWithDistanceParam)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "lane_change_action_right_sine_distance.xosc"};

    const double ego_start_y_position = -1.75;
    const double traffic_y_position = -5.25;

    InitSimulator("FastestRun.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement);
    SpinNTimes(1);

    // check ego's and traffic's original state
    ASSERT_EQ(GetLastGroundTruth().moving_object_size(), 2);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(0).base().position().y(), ego_start_y_position);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(1).base().position().y(), traffic_y_position);

    const double step_of_lane_change = 971;
    const double step_of_lane_change_complete = 1099;

    SpinNTimes(step_of_lane_change - 1);

    std::vector<std::pair<double, double>> trajectory_xy;

    // store the trajectory
    for (auto itr = step_of_lane_change; itr <= step_of_lane_change_complete; ++itr)
    {
        // capture the points in y direction as function of adjusted x
        auto x = GetLastGroundTruth().moving_object(0).base().position().x();
        auto y = GetLastGroundTruth().moving_object(0).base().position().y();
        trajectory_xy.emplace_back(x, y);
        SpinNTimes(1);
    }
    auto end_idx = trajectory_xy.size() - 1;

    // check trajectory nature
    for (const auto& each : trajectory_xy)
    {
        sineCheck(each.first, each.second, end_idx, trajectory_xy);
    }
    // check ego's state after lane change, it shall reach traffic position in y direction
    EXPECT_NEAR(trajectory_xy[end_idx].second, traffic_y_position, kSineYTolerance);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithLaneChangeAction_WhenEgoIsCloseToTraffic_ThenItChangesLaneToLeftAsSineWaveInYDirectionWithRateParam)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "lane_change_action_left_sine_rate.xosc"};

    const double ego_start_y_position = -5.25;
    const double traffic_y_position = -1.75;

    InitSimulator("FastestRun.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement);
    SpinNTimes(1);

    // check ego's and traffic's original state
    ASSERT_EQ(GetLastGroundTruth().moving_object_size(), 2);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(0).base().position().y(), ego_start_y_position);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(1).base().position().y(), traffic_y_position);

    const double step_of_lane_change = 971;
    const double step_of_lane_change_complete = 1157;

    SpinNTimes(step_of_lane_change - 1);

    std::vector<std::pair<double, double>> trajectory_xy;

    // store the trajectory
    for (auto itr = step_of_lane_change; itr <= step_of_lane_change_complete; ++itr)
    {
        // capture the points in y direction as function of adjusted x
        auto x = GetLastGroundTruth().moving_object(0).base().position().x();
        auto y = GetLastGroundTruth().moving_object(0).base().position().y();
        trajectory_xy.emplace_back(x, y);
        SpinNTimes(1);
    }

    auto end_idx = trajectory_xy.size() - 1;

    // check trajectory nature
    for (const auto& each : trajectory_xy)
    {
        sineCheck(each.first, each.second, end_idx, trajectory_xy);
    }

    // check ego's state after lane change, it shall reach traffic position in y direction
    EXPECT_NEAR(trajectory_xy[end_idx].second, traffic_y_position, kSineYTolerance);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithLaneChangeAction_WhenEgoIsCloseToTraffic_ThenItChangesLaneToRightAsSineWaveInYDirectionWithRateParam)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "lane_change_action_right_sine_rate.xosc"};

    const double ego_start_y_position = -1.75;
    const double traffic_y_position = -5.25;

    InitSimulator("FastestRun.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement);
    SpinNTimes(1);

    // check ego's and traffic's original state
    ASSERT_EQ(GetLastGroundTruth().moving_object_size(), 2);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(0).base().position().y(), ego_start_y_position);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(1).base().position().y(), traffic_y_position);

    const double step_of_lane_change = 971;
    const double step_of_lane_change_complete = 1157;

    SpinNTimes(step_of_lane_change - 1);

    std::vector<std::pair<double, double>> trajectory_xy;

    // store the trajectory
    for (auto itr = step_of_lane_change; itr <= step_of_lane_change_complete; ++itr)
    {
        // capture the points in y direction as function of adjusted x
        auto x = GetLastGroundTruth().moving_object(0).base().position().x();
        auto y = GetLastGroundTruth().moving_object(0).base().position().y();
        trajectory_xy.emplace_back(x, y);
        SpinNTimes(1);
    }
    auto end_idx = trajectory_xy.size() - 1;

    // check trajectory nature
    for (const auto& each : trajectory_xy)
    {
        sineCheck(each.first, each.second, end_idx, trajectory_xy);
    }
    // check ego's state after lane change, it shall reach traffic position in y direction
    EXPECT_NEAR(trajectory_xy[end_idx].second, traffic_y_position, kSineYTolerance);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithLaneChangeAction_WhenEgoIsCloseToTraffic_ThenItChangesLaneToLeftAsSineWaveInYDirectionWithTimeParam)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "lane_change_action_left_sine_time.xosc"};

    const double ego_start_y_position = -5.25;
    const double traffic_y_position = -1.75;

    InitSimulator("FastestRun.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement);
    SpinNTimes(1);

    // check ego's and traffic's original state
    ASSERT_EQ(GetLastGroundTruth().moving_object_size(), 2);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(0).base().position().y(), ego_start_y_position);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(1).base().position().y(), traffic_y_position);

    const double step_of_lane_change = 971;
    const double step_of_lane_change_complete = 1073;

    SpinNTimes(step_of_lane_change - 1);

    std::vector<std::pair<double, double>> trajectory_xy;

    // store the trajectory
    for (auto itr = step_of_lane_change; itr <= step_of_lane_change_complete; ++itr)
    {
        // capture the points in y direction as function of adjusted x
        auto x = GetLastGroundTruth().moving_object(0).base().position().x();
        auto y = GetLastGroundTruth().moving_object(0).base().position().y();
        trajectory_xy.emplace_back(x, y);
        SpinNTimes(1);
    }
    auto end_idx = trajectory_xy.size() - 1;

    // check trajectory nature
    for (const auto& each : trajectory_xy)
    {
        sineCheck(each.first, each.second, end_idx, trajectory_xy);
    }
    // check ego's state after lane change, it shall reach traffic position in y direction
    EXPECT_NEAR(trajectory_xy[end_idx].second, traffic_y_position, kSineYTolerance);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithLaneChangeAction_WhenEgoIsCloseToTraffic_ThenItChangesLaneToRightAsSineWaveInYDirectionWithTimeParam)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "lane_change_action_right_sine_time.xosc"};

    const double ego_start_y_position = -1.75;
    const double traffic_y_position = -5.25;

    InitSimulator("FastestRun.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement);
    SpinNTimes(1);

    // check ego's and traffic's original state
    ASSERT_EQ(GetLastGroundTruth().moving_object_size(), 2);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(0).base().position().y(), ego_start_y_position);
    EXPECT_DOUBLE_EQ(GetLastGroundTruth().moving_object(1).base().position().y(), traffic_y_position);

    const double step_of_lane_change = 971;
    const double step_of_lane_change_complete = 1073;

    SpinNTimes(step_of_lane_change - 1);

    std::vector<std::pair<double, double>> trajectory_xy;

    // store the trajectory
    for (auto itr = step_of_lane_change; itr <= step_of_lane_change_complete; ++itr)
    {
        // capture the points in y direction as function of adjusted x
        auto x = GetLastGroundTruth().moving_object(0).base().position().x();
        auto y = GetLastGroundTruth().moving_object(0).base().position().y();
        trajectory_xy.emplace_back(x, y);
        SpinNTimes(1);
    }
    auto end_idx = trajectory_xy.size() - 1;

    // check trajectory nature
    for (const auto& each : trajectory_xy)
    {
        sineCheck(each.first, each.second, end_idx, trajectory_xy);
    }
    // check ego's state after lane change, it shall reach traffic position in y direction
    EXPECT_NEAR(trajectory_xy[end_idx].second, traffic_y_position, kSineYTolerance);
}

}  // namespace gtgen::simulator
