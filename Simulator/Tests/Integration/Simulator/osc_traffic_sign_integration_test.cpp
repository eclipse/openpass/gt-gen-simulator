/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithMapContainingTrafficSigns_WhenSimulatorSteps_ThenGroundTruthContainsTheTrafficSigns)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_traffic_signs.xosc"};

    const auto expected_count_of_main_signs = 23;
    const auto expected_count_of_supplementary_signs = 4;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_main_signs, last_ground_truth.traffic_sign_size());

    auto actual_count_of_supplementary_signs = 0;
    for (auto i = 0; i < expected_count_of_main_signs; i++)
    {
        actual_count_of_supplementary_signs += last_ground_truth.traffic_sign(i).supplementary_sign_size();
    }

    EXPECT_EQ(expected_count_of_supplementary_signs, actual_count_of_supplementary_signs);
}

}  // namespace gtgen::simulator
