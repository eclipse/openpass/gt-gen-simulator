/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithJunction_WhenSimulatorSteps_ThenGroundTruthOfLogicalLaneIsConvertedCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_junction.xosc"};

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(last_ground_truth.logical_lane_size(), 7);
    ASSERT_EQ(last_ground_truth.lane_size(), 7);
    ASSERT_EQ(last_ground_truth.reference_line_size(), 3);

    const auto lanes = last_ground_truth.lane();
    const auto logical_lanes = last_ground_truth.logical_lane();
    const auto reference_lanes = last_ground_truth.reference_line();

    const auto& road_2_lane_minus_1_logical_lane = logical_lanes[0];
    const auto& road_1_lane_minus_1_logical_lane = logical_lanes[1];
    const auto& road_1_lane_minus_2_logical_lane = logical_lanes[2];
    const auto& road_1_lane_minus_3_logical_lane = logical_lanes[3];
    const auto& road_0_lane_1_logical_lane = logical_lanes[4];
    const auto& road_0_lane_2_logical_lane = logical_lanes[5];
    const auto& road_0_lane_3_logical_lane = logical_lanes[6];

    const auto& road_2_reference_line = reference_lanes[0];
    const auto& road_1_reference_line = reference_lanes[1];
    const auto& road_0_reference_line = reference_lanes[2];

    const auto road_2_lane_minus_1 = lanes[0];
    const auto road_1_lane_minus_2 = lanes[2];
    const auto road_0_lane_2 = lanes[5];

    // road 2
    ASSERT_EQ(road_2_lane_minus_1_logical_lane.left_boundary_id_size(), 1);
    ASSERT_EQ(road_2_lane_minus_1_logical_lane.right_boundary_id_size(), 1);

    EXPECT_EQ(road_2_lane_minus_1_logical_lane.right_adjacent_lane_size(), 0);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane.left_adjacent_lane_size(), 0);

    ASSERT_EQ(road_2_lane_minus_1_logical_lane.predecessor_lane_size(), 1);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane.predecessor_lane().at(0).other_lane_id(),
              road_0_lane_1_logical_lane.id());
    EXPECT_EQ(road_2_lane_minus_1_logical_lane.successor_lane_size(), 0);

    ASSERT_EQ(road_2_lane_minus_1_logical_lane.physical_lane_reference_size(), 1);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane.physical_lane_reference().at(0).physical_lane_id(),
              road_2_lane_minus_1.id());

    EXPECT_EQ(road_2_lane_minus_1_logical_lane.has_reference_line_id(), true);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane.reference_line_id(), road_2_reference_line.id());

    EXPECT_EQ(road_2_lane_minus_1_logical_lane.type(), osi3::LogicalLane::Type::LogicalLane_Type_TYPE_UNKNOWN);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane.move_direction(),
              osi3::LogicalLane::MoveDirection::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);

    // road 1
    ASSERT_EQ(road_1_lane_minus_2_logical_lane.left_boundary_id_size(), 1);
    ASSERT_EQ(road_1_lane_minus_2_logical_lane.right_boundary_id_size(), 1);

    EXPECT_EQ(road_1_lane_minus_2_logical_lane.right_adjacent_lane_size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane.right_adjacent_lane().at(0).other_lane_id(),
              road_1_lane_minus_3_logical_lane.id());

    EXPECT_EQ(road_1_lane_minus_2_logical_lane.left_adjacent_lane_size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane.left_adjacent_lane().at(0).other_lane_id(),
              road_1_lane_minus_1_logical_lane.id());

    ASSERT_EQ(road_1_lane_minus_2_logical_lane.predecessor_lane_size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane.predecessor_lane().at(0).other_lane_id(),
              road_0_lane_2_logical_lane.id());
    EXPECT_EQ(road_1_lane_minus_2_logical_lane.successor_lane_size(), 0);

    ASSERT_EQ(road_1_lane_minus_2_logical_lane.physical_lane_reference_size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane.physical_lane_reference().at(0).physical_lane_id(),
              road_1_lane_minus_2.id());

    EXPECT_EQ(road_1_lane_minus_1_logical_lane.has_reference_line_id(), true);
    EXPECT_EQ(road_1_lane_minus_1_logical_lane.reference_line_id(), road_1_reference_line.id());

    EXPECT_EQ(road_1_lane_minus_2_logical_lane.type(), osi3::LogicalLane::Type::LogicalLane_Type_TYPE_BORDER);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane.move_direction(),
              osi3::LogicalLane::MoveDirection::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S);

    // road 0
    ASSERT_EQ(road_0_lane_2_logical_lane.left_boundary_id_size(), 1);
    ASSERT_EQ(road_0_lane_2_logical_lane.right_boundary_id_size(), 1);

    EXPECT_EQ(road_0_lane_2_logical_lane.right_adjacent_lane_size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane.right_adjacent_lane().at(0).other_lane_id(), road_0_lane_1_logical_lane.id());

    EXPECT_EQ(road_0_lane_2_logical_lane.left_adjacent_lane_size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane.left_adjacent_lane().at(0).other_lane_id(), road_0_lane_3_logical_lane.id());

    EXPECT_EQ(road_0_lane_2_logical_lane.predecessor_lane_size(), 0);
    ASSERT_EQ(road_0_lane_2_logical_lane.successor_lane_size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane.successor_lane().at(0).other_lane_id(), road_1_lane_minus_2_logical_lane.id());

    ASSERT_EQ(road_0_lane_2_logical_lane.physical_lane_reference_size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane.physical_lane_reference().at(0).physical_lane_id(), road_0_lane_2.id());

    EXPECT_EQ(road_0_lane_1_logical_lane.has_reference_line_id(), true);
    EXPECT_EQ(road_0_lane_1_logical_lane.reference_line_id(), road_0_reference_line.id());

    EXPECT_EQ(road_0_lane_2_logical_lane.type(), osi3::LogicalLane::Type::LogicalLane_Type_TYPE_BORDER);
    EXPECT_EQ(road_0_lane_2_logical_lane.move_direction(),
              osi3::LogicalLane::MoveDirection::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S);
}

}  // namespace gtgen::simulator
