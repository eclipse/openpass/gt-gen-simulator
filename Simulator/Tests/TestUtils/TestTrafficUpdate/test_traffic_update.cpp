/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Tests/TestUtils/TestTrafficUpdate/test_traffic_update.h"

#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"

#include <optional>

namespace gtgen::simulator::test_utils
{

using gtgen::core::test_utils::GetHostVehicle;

TestTrafficUpdate::TestTrafficUpdate(TestTrafficUpdate::TestHostVehicleMovement test_host_vehicle_movement)
    : test_host_vehicle_movement_(test_host_vehicle_movement)
{
    auto* moving_object = traffic_update_.add_update();
    moving_object->mutable_id()->set_value(0);
}

void TestTrafficUpdate::Step(const osi3::GroundTruth& gt)
{
    ModifyVehicleState(gt);
}

namespace
{
std::optional<std::reference_wrapper<osi3::MovingObject>> GetMovingObjectById(osi3::TrafficUpdate& gt, std::uint64_t id)
{
    for (auto& object : *gt.mutable_update())
    {
        if (object.id().value() == id)
        {
            return std::reference_wrapper<osi3::MovingObject>(object);
        }
    }
    return std::nullopt;
}
}  // namespace

void TestTrafficUpdate::ModifyVehicleState(const osi3::GroundTruth& gt)
{
    switch (test_host_vehicle_movement_)
    {
        case TestHostVehicleMovement::kExternalDriver:
        {
            break;
        }
        case TestHostVehicleMovement::kSimpleStub:
        {
            const auto host_vehicle = GetHostVehicle(gt).value();
            auto moving_object = GetMovingObjectById(traffic_update_, host_vehicle.id().value());

            moving_object->get().mutable_base()->mutable_velocity()->set_y(host_vehicle.base().velocity().y() + 0.01);
            moving_object->get().mutable_base()->mutable_position()->set_y(host_vehicle.base().position().y() + 0.1);
            moving_object->get().mutable_base()->mutable_orientation()->set_yaw(1.5708);

            break;
        }
        case TestHostVehicleMovement::kInternalMovement:
        {
            const auto host_vehicle = GetHostVehicle(gt).value();
            auto moving_object = GetMovingObjectById(traffic_update_, host_vehicle.id().value());

            moving_object->get().mutable_base()->mutable_velocity()->CopyFrom(host_vehicle.base().velocity());
            moving_object->get().mutable_base()->mutable_acceleration()->CopyFrom(host_vehicle.base().acceleration());
            moving_object->get().mutable_base()->mutable_position()->CopyFrom(host_vehicle.base().position());
            moving_object->get().mutable_base()->mutable_orientation()->CopyFrom(host_vehicle.base().orientation());

            break;
        }
        default:
            break;
    }

    traffic_update_.mutable_timestamp()->set_seconds(traffic_update_.mutable_timestamp()->seconds() + 1);
}

}  // namespace gtgen::simulator::test_utils
