/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/ScenarioEngines/OscScenarioEngine/Engine/osc_scenario_engine.h"

#include <MantleAPI/Test/test_utils.h>
#include <TestUtils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/catalog/CatalogHelperV1_3.h>

using namespace std::string_literals;
namespace gtgen::simulator::osc_scenario_engine::engine
{

namespace fs = gtgen::core::fs;

class OscScenarioEngineTest : public testing::OpenScenarioEngine::v1_3::OpenScenarioEngineTestBase
{
  protected:
    void SetUp() override
    {
        testing::OpenScenarioEngine::v1_3::OpenScenarioEngineTestBase::SetUp();
        ON_CALL(controller_, GetName()).WillByDefault(testing::ReturnRef(controller_name_));
        ON_CALL(controller_, GetUniqueId()).WillByDefault(testing::Return(1234));

        data_path_handler_.SetGtGenDataDirectory(relative_data_path);
        data_path_handler_.SetBasePath(osc_path);
    }

    const fs::path relative_data_path{fs::current_path() / "../open_scenario_engine/tests/data/"};
    const fs::path osc_path{relative_data_path / "Scenarios/AutomatedLaneKeepingSystemScenarios"};
    const fs::path osc_file_path{osc_path / "ALKS_Scenario_4.1_1_FreeDriving_TEMPLATE.xosc"};
    const fs::path invalid_osc_file_path{osc_path / "Invalid.xosc"};
    simulation::user_data::DataPathHandler data_path_handler_{};
    std::string controller_name_{"TestController"s};
};

TEST_F(OscScenarioEngineTest, GivenValidScenarioFileInGtGenData_WhenInitializedAndSteppedOnce_ThenNoThrow)
{
    OSCScenarioEngine engine(osc_file_path, data_path_handler_, env_);
    EXPECT_NO_THROW(engine.Init());
    EXPECT_NO_THROW(engine.SetupDynamicContent());
    EXPECT_NO_THROW(engine.Step(mantle_api::Time(1)));
}

TEST_F(OscScenarioEngineTest, GivenInvalidScenarioFile_WhenInitialized_ThenThrowAndLogErrors)
{
    testing::internal::CaptureStdout();

    OSCScenarioEngine engine(invalid_osc_file_path, data_path_handler_, env_);
    EXPECT_ANY_THROW(engine.Init());

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr("Scenario validation failed with"));
}

TEST_F(OscScenarioEngineTest, GivenNonExistingScenarioFile_WhenInitialized_ThenThrow)
{
    OSCScenarioEngine engine("not-existing-file.xosc", data_path_handler_, env_);
    EXPECT_ANY_THROW(engine.Init());
}

TEST_F(OscScenarioEngineTest, GivenExistingScenarioFileWithWrongCatalogName_WhenValidateScenario_ThenReturnsErrors)
{
    OSCScenarioEngine engine(
        "./Simulator/Tests/Data/Scenarios/XOSC/scenario_with_wrong_catalog_path.xosc", data_path_handler_, env_);
    EXPECT_EQ(3, engine.ValidateScenario());
}

TEST_F(OscScenarioEngineTest, GivenExistingValidScenarioFile_WhenValidateScenario_ThenReturnsZero)
{
    OSCScenarioEngine engine("ALKS_Scenario_4.1_1_FreeDriving_TEMPLATE.xosc", data_path_handler_, env_);
    EXPECT_EQ(0, engine.ValidateScenario());
}

}  // namespace gtgen::simulator::osc_scenario_engine::engine
