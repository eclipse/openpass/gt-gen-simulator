/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/ScenarioEngines/ScenarioEngineFactory/scenario_engine_factory.h"

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Simulator/ScenarioEngines/OscScenarioEngine/Engine/osc_scenario_engine.h"
#include "Simulator/Utils/UserData/user_data_manager.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_scenario_engine.h>

namespace gtgen::simulator
{

namespace fs = gtgen::core::fs;
using gtgen::core::simulation::simulator::SimulatorException;

std::unique_ptr<mantle_api::IScenarioEngine> ScenarioEngineFactory::Create(
    const std::string& file,
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    simulation::user_data::UserDataManager* user_data_manager)
{
    fs::path file_path{file};
    if (file_path.extension() == ".xosc")
    {
        return std::make_unique<osc_scenario_engine::OSCScenarioEngine>(
            file, user_data_manager->GetPathHandler(), environment);
    }
    else
    {
        throw SimulatorException("Unsupported scenario file extension: '{}'. ", file_path.extension().string());
    }
}

}  // namespace gtgen::simulator
