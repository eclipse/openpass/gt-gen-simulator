/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_SCENARIOENGINEFACTORY_SCENARIOENGINEFACTORY_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_SCENARIOENGINEFACTORY_SCENARIOENGINEFACTORY_H

#include <MantleAPI/Common/time_utils.h>

#include <memory>
#include <string>

namespace mantle_api
{
class IScenarioEngine;
class IEnvironment;
}  // namespace mantle_api

namespace gtgen::simulator::simulation::user_data
{
class UserDataManager;
}

namespace gtgen::simulator
{

class ScenarioEngineFactory
{
  public:
    static std::unique_ptr<mantle_api::IScenarioEngine> Create(
        const std::string& file,
        const std::shared_ptr<mantle_api::IEnvironment>& environment,
        simulation::user_data::UserDataManager* user_data_manager);
};

}  // namespace gtgen::simulator

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_SCENARIOENGINEFACTORY_SCENARIOENGINEFACTORY_H
