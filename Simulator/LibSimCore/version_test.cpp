/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Export/version.h"

#include <gtest/gtest.h>

namespace gtgen::simulator
{

TEST(VersionTest, GivenExportedGetSimulatorVersion_WhenCallingTheFunction_ThenLinkingShallWorkAndNonEmptyStringReturned)
{
    EXPECT_FALSE(GetSimulatorVersion().empty());
}

}  // namespace gtgen::simulator
