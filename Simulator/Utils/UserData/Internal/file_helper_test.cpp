/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/Internal/file_helper.h"

#include <gtest/gtest.h>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;
using gtgen::core::Version;

namespace
{
// Helper since GTest seems to handle aggregate initialization improperly
constexpr Version MakeVersion(std::int32_t major, std::int32_t minor, std::int32_t patch)
{
    return Version{major, minor, patch};
}

}  // namespace

TEST(FileHelperTest, GivenValidVersion_WhenExtractVersionFromString_ThenVersionExtracted)
{
    EXPECT_EQ(MakeVersion(2019, 1, 0), ExtractVersionFromString("2019.1.0", "dummy_file.txt"));
    EXPECT_EQ(MakeVersion(2020, 10, 01), ExtractVersionFromString("2020.10.01", "dummy_file.txt"));
    EXPECT_EQ(MakeVersion(2019, 1234, 354), ExtractVersionFromString("2019.1234.354", "dummy_file.txt"));
    EXPECT_EQ(MakeVersion(209, 1, 0), ExtractVersionFromString("209.1.0", "dummy_file.txt"));
    EXPECT_EQ(MakeVersion(8, 2, 2), ExtractVersionFromString("8.2.2", "dummy_file.txt"));
}

TEST(FileHelperTest, GivenMalformedVersion_WhenExtractVersionFromString_ThenVersionIsAllZero)
{
    EXPECT_EQ(MakeVersion(0, 0, 0), ExtractVersionFromString("20200.1.1", "dummy_file.txt"));
    EXPECT_EQ(MakeVersion(0, 0, 0), ExtractVersionFromString("some other info 2019.0.1", "dummy_file.txt"));
    EXPECT_EQ(MakeVersion(0, 0, 0), ExtractVersionFromString("2019.0.1 # with comment", "dummy_file.txt"));
}

TEST(FileHelperTest, GivenVersionFileCreated_WhenReadVersionFile_ThenVersionCorrectlyWrittenAndRead)
{
    fs::path test_gtgen_data = "Simulator/Tests/TestGtGenData";
    fs::remove_all(test_gtgen_data);
    fs::create_directory(test_gtgen_data);

    Version gtgen_version{1999, 2, 3};
    CreateOrUpgradeVersionFile(test_gtgen_data, gtgen_version);
    const auto written_version = ReadVersionFile(test_gtgen_data);

    EXPECT_EQ(written_version, gtgen_version);
}

}  // namespace gtgen::simulator::simulation::user_data
