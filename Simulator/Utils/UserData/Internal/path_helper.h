/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_PATHHELPER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_PATHHELPER_H

#include "Core/Service/FileSystem/filesystem.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

fs::path ResolveInstallationSourcePath();

bool IsMetaFile(const fs::path& path);
bool IsTestDirectory(const fs::path& path);

}  // namespace gtgen::simulator::simulation::user_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_PATHHELPER_H
