/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_UTILS_LOGGING_LOGGING_H
#define GTGEN_UTILS_LOGGING_LOGGING_H

#include "Core/Service/Logging/logging_template.h"
#include "Core/Service/MantleApiExtension/formatting.h"

namespace gtgen::simulator
{

/// @brief GTGEN LOGGING
///
/// Logging will be redirected to the console and (optionally) to a logfile.
///
/// If GTGEN is build in Debug the trace log levels are enabled, otherwise the trace log calls are optimized
/// away at compile time (via Preprocessor-Macro).
///
/// In the section below in this file the loggers are setup. Therefore a convenience macro exists, which
/// maps the namespace to a logger-name and creates the logging functions within the given namespace.
/// This allows easy usage of the logging:
/// - Include this file
/// - The logging functions are available in the current namespace (if the namespace has been setup in this file).
///
/// The following logging mechanisms are supported:
/// - TRACE()
/// - Debug()
/// - Info()
/// - Warn()
/// - Error()
/// The general distinction between a macro call and function call is that macro calls will log the filename /
/// line number, whereas log functions only display the default log pattern and the message.
///
/// If you want to log an error message when you throw an exception, you can use one of the two:
/// - LogAndThrow<ExceptionType>(string-message):
///           Logs string-message to error and throws exception. If possible, string-message will be used as constructor
///           argument for ExceptionType
///
/// - LOG_AND_THROW_WITH_FILE_DETAILS(exception)
///           Requires exception to provide a `string_like what()` function, logs it to error including filename and
///           filenumber, and throws the exception expression.
///
/// With this setup, it is possible to call e.g. Info("Hello") from anywhere within the package (if the header is
/// included) and this will automatically add the correct package name in square brackets to the log, e.g.:
///
/// [2019-04-12 08:34:53.916] [environment] [info] Some important logging within the environment::map namespace

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - designed like this, OK
#define ENABLE_NAMESPACE_LOGGING(name_space, logger_name) \
    namespace name_space                                  \
    {                                                     \
    ENABLE_PACKAGE_LOGGING(logger_name)                   \
    }

// ######################################################################################
//  Register all namespaces which shall have their own loggers here.
//  This template will create the logging functions for the given namespace name and map it to a logger name
// ######################################################################################
ENABLE_PACKAGE_LOGGING("gtgen::simulator")  // setup the default logger

// Map Engines
ENABLE_NAMESPACE_LOGGING(environment, "environment")

// Scenario Engines
ENABLE_NAMESPACE_LOGGING(osc_scenario_engine, "osc_engine")

}  // namespace gtgen::simulator

#endif  // GTGEN_UTILS_LOGGING_LOGGING_H
