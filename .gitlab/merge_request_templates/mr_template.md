**Summary**
<!-- Describe the purpose of the PR and what it changes. -->

**Screenshots  (if applicable)**
<!-- Add screenshots or GIFs to demonstrate the change -->

<hr>

**Checklist Before Merging**

1. Third-Party Library and Version Updates
- [ ] All third-party libraries have been updated (e.g., `road-logic-suite`, `gt-gen-core`, `osc1-engine`, `mantle-API`).
- [ ] `gt-gen-core` version number has been updated in `Core/update_version.patch` (if `gt-gen-core` is updated).
- [ ] `Notice.md` has been updated with relevant changes.
- [ ] The version number in `Simulator/LibSimCore/version.bzl` has been updated accordingly.

2. Documentation Updates
- [ ] User-facing documentation (user guides, tutorials, etc.) has been updated to reflect the changes.
- [ ] The feature list in `Documentation/03_scenario_description/supported_osc_features.md` has been updated.

3. Tests and CI Checks
- [ ] Unit tests / integration tests are added
- [ ] All CI checks are passing <!-- Link to CI Job -->

<hr>

**Related Issues / Tickets**
<!-- Link to related issues, tasks, or tickets -->
