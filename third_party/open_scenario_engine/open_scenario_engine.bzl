load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v13.0.0"

def open_scenario_engine():
    maybe(
        http_archive,
        name = "open_scenario_engine",
        url = "https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/archive/{tag}/openscenario1_engine-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "e29084b8c809c1d4e68a350db9f55a254ea1492cf1d6c9100d8f03403d535c43",
        strip_prefix = "openscenario1_engine-{tag}/engine".format(tag = _TAG),
        type = "tar.gz",
        patch_args = ["-p1"],
    )
