"""
This file contains the necessary information to produce the SBOM report.
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_NAME = "clara"
_VERSION = "1.1.5"
_PATH = "https://github.com/catchorg/Clara/archive/refs/tags/v{version}.zip".format(version = _VERSION)
_COPYRIGHT = "Copyright 2017 Two Blue Cubes Ltd. All rights reserved."

def clara():
    maybe(
        repo_rule = http_archive,
        name = _NAME,
        url = _PATH,
        build_file = "//third_party/clara:clara.BUILD",
        strip_prefix = "Clara-{version}".format(version = _VERSION),
    )
