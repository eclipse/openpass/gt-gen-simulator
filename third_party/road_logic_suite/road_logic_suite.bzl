load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "v4.1.4"

def road_logic_suite():
    maybe(
        http_archive,
        name = "road_logic_suite",
        build_file = "//third_party/road_logic_suite:road_logic_suite.BUILD",
        sha256 = "405e3495164a95c103385471473239b2e21abee13a4afdf2ced89420603db4d2",
        urls = [
            "https://ci.eclipse.org/openpass/job/RoadLogicSuite/view/tags/job/{version}/lastSuccessfulBuild/artifact/artifacts/road-logic-suite-{version}-linux-x86_64.tar.gz".format(version = _VERSION),
        ],
    )
