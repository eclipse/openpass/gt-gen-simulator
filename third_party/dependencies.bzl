"""A module defining the third party dependencies for instantiating in WORKSPACE"""

load("@gt_gen_core//third_party/open_simulation_interface:open_simulation_interface.bzl", "open_simulation_interface")
load("@gt_gen_core//third_party/osi_traffic_participant:osi_traffic_participant.bzl", "osi_traffic_participant")
load("//third_party/clara:clara.bzl", "clara")
load("//third_party/open_scenario_engine:open_scenario_engine.bzl", "open_scenario_engine")
load("//third_party/open_scenario_parser:open_scenario_parser.bzl", "open_scenario_parser")
load("//third_party/openssl:openssl.bzl", "openssl")
load("//third_party/road_logic_suite:road_logic_suite.bzl", "road_logic_suite")
load("//third_party/stochastics_library:stochastics_library.bzl", "stochastics_library")
load("//third_party/yase:yase.bzl", "yase")

def third_party_deps_internal():
    clara()
    open_scenario_engine()
    open_scenario_parser()
    road_logic_suite()
    yase()
    osi_traffic_participant()
    open_simulation_interface()
    stochastics_library()

def override_openssl():
    openssl()
